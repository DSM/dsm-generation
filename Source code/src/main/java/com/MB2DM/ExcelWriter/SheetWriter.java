package com.MB2DM.ExcelWriter;

import static com.MB2DM.Constants.Constants.*;

import java.io.File;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.MB2DM.MatrixMaker.DMMMaker;
import com.MB2DM.MatrixMaker.MDMMaker;
import com.MB2DM.MatrixMaker.Matrix;

/**
 * Writes DSM, DMM and MDM as matrices in xlsx files
 */
public class SheetWriter {

    /**
     * Write the content of a Matrix object in an xlsx file
     * 
     * @param matrix Matrix object that must be written
     */

    public void writeData(Matrix matrix) throws IOException {
        // Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        // Create a blank sheet
        XSSFSheet sheet = workbook.createSheet(matrix.getName());

        if (matrix.getType().compareTo(MDM) == 0) {

            int rownum = 0;

            for (int i = 0; i < matrix.getMatrix().size(); i++) {
                // create a row of excelsheet
                Row row = sheet.createRow(rownum++);

                // get object array of prerticuler key
                ArrayList<String> elementArr = matrix.getMatrix().get(i);

                int cellnum = 0;

                for (String string : elementArr) {
                    XSSFCellStyle style = workbook.createCellStyle();
                    if (rownum > 1 && rownum < 2 + matrix.getDiagramsSize().get(FUNCTION_PART) && cellnum > 0
                            && cellnum < 1 + matrix.getDiagramsSize().get(FUNCTION_PART)) {
                        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    } else if (rownum > 1 + matrix.getDiagramsSize().get(FUNCTION_PART)
                            && rownum < 2 + matrix.getDiagramsSize().get(FUNCTION_PART)
                                    + matrix.getDiagramsSize().get(COMPONENT_PART)
                            && cellnum > matrix.getDiagramsSize().get(FUNCTION_PART)
                            && cellnum < 1 + matrix.getDiagramsSize().get(FUNCTION_PART)
                                    + matrix.getDiagramsSize().get(COMPONENT_PART)) {
                        style.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    } else if (rownum > 1 + matrix.getDiagramsSize().get(FUNCTION_PART)
                            + matrix.getDiagramsSize().get(COMPONENT_PART)
                            && rownum < 2 + matrix.getDiagramsSize().get(FUNCTION_PART)
                                    + matrix.getDiagramsSize().get(COMPONENT_PART)
                                    + matrix.getDiagramsSize().get(REQUIREMENT_PART)
                            && cellnum > matrix.getDiagramsSize().get(FUNCTION_PART)
                                    + matrix.getDiagramsSize().get(COMPONENT_PART)
                            && cellnum < 1 + matrix.getDiagramsSize().get(FUNCTION_PART)
                                    + matrix.getDiagramsSize().get(COMPONENT_PART)
                                    + matrix.getDiagramsSize().get(REQUIREMENT_PART)) {
                        style.setFillForegroundColor(IndexedColors.TAN.getIndex());
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    } else if (rownum == 1 || cellnum == 0) {
                        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    } else {
                    }
                    Cell cell = row.createCell(cellnum++);
                    cell.setCellValue(string);
                    cell.setCellStyle(style);
                }

            }
        }

        else if (matrix.getType().compareTo(DSM) == 0) {

            byte[] rgb = new byte[3];
            rgb[0] = (byte) 225; // red
            rgb[1] = (byte) 225; // green
            rgb[2] = (byte) 225; // blue
            XSSFColor color = new XSSFColor(rgb, new DefaultIndexedColorMap());

            int rownum = 0;
            for (int i = 0; i < matrix.getMatrix().size(); i++) {
                // create a row of excelsheet
                Row row = sheet.createRow(rownum++);

                // get object array of prerticuler key
                ArrayList<String> elementArr = matrix.getMatrix().get(i);

                int cellnum = 0;

                for (String string : elementArr) {
                    XSSFCellStyle style = workbook.createCellStyle();

                    if (rownum == 1 || cellnum == 0) {
                        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    } else if (rownum > 1 && rownum == cellnum + 1) {
                        style.setFillForegroundColor(color);
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    }

                    Cell cell = row.createCell(cellnum++);
                    cell.setCellValue(string);
                    cell.setCellStyle(style);
                }

            }
        }

        else {
            int rownum = 0;
            for (int i = 0; i < matrix.getMatrix().size(); i++) {
                // create a row of excelsheet
                Row row = sheet.createRow(rownum++);

                // get object array of prerticuler key
                ArrayList<String> elementArr = matrix.getMatrix().get(i);

                int cellnum = 0;

                for (String string : elementArr) {
                    XSSFCellStyle style = workbook.createCellStyle();

                    if (rownum == 1 || cellnum == 0) {
                        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    }

                    Cell cell = row.createCell(cellnum++);
                    cell.setCellValue(string);
                    cell.setCellStyle(style);
                }

            }
        }

        // Write the workbook in file system
        FileOutputStream out = new FileOutputStream(
                new File("design_matrices/" + matrix.getType() + "/" + matrix.getName() + XLSX));
        workbook.write(out);
        out.close();
        System.out.println(matrix.getName() + XLSX + " successfully written on disk.");

        workbook.close();

    }

    /**
     * Write the MDM of a MDMMaker object
     * 
     * @param mdm MDMMaker object that must be written
     */
    public void writeData(MDMMaker mdm) throws IOException {

        Matrix binary = mdm.getBinaryMDM();
        this.writeData(binary);

        Matrix directed = mdm.getDirectedMDM();
        this.writeData(directed);

    }

    /**
     * Write the DMM of a DMMMaker object
     * 
     * @param dmm DMMMaker object that must be written
     */
    public void writeData(DMMMaker dmm) throws IOException {

        for (String matrix : dmm.getBinaryMatrixList().keySet()) {
            this.writeData(dmm.getBinaryMatrixList().get(matrix));
        }

        for (String matrix : dmm.getDirectedMatrixList().keySet()) {
            this.writeData(dmm.getDirectedMatrixList().get(matrix));
        }
    }


}
