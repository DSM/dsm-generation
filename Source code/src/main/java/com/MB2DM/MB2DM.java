package com.MB2DM;

import org.jdom2.JDOMException;

import java.io.IOException;

import com.MB2DM.UserCommand.UserCommand;

/**
 * Class that must be launched. Calls a GUI.
 */
public class MB2DM {

    public static void main(String[] args)
            throws JDOMException, IOException, ClassNotFoundException, InterruptedException {

        UserCommand uc = new UserCommand();
        uc.userInterface();

    }

}
