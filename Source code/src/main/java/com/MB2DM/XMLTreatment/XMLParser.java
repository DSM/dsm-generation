package com.MB2DM.XMLTreatment;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;

/**
 * Extract the root element of an XML tree structure
 */
public class XMLParser {

    /**
     * root of the XML tree structure
     */
    private Element rootNode;

    /**
     * Constructor that extract the rootnode of an XML document
     * 
     * @param filepath path of the XML document to parse
     */
    public XMLParser(String filepath) {
        try {

            SAXBuilder sax = new SAXBuilder();
            // XML is a local file
            Document doc = sax.build(new File(filepath));

            this.rootNode = doc.getRootElement();

        } catch (IOException | JDOMException e) {
            e.printStackTrace();
        }

    }

    /**
     * rootnode getter
     * 
     * @return rootnode
     */
    public Element getRootNode() {
        return this.rootNode;
    }

    /**
     * rootnode setter
     * 
     * @param rootNode root of the XML structure
     */
    public void setRootNode(Element rootNode) {
        this.rootNode = rootNode;
    }

}
