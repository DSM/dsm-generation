package com.MB2DM.XMLTreatment;

import static com.MB2DM.Constants.Constants.*;

import com.MB2DM.Analyse.FullModel.FullModel;
import com.MB2DM.Analyse.ModelParts.Components;
import com.MB2DM.Analyse.ModelParts.Functions;
import com.MB2DM.Analyse.ModelParts.Requirements;

import org.jdom2.Element;

/**
 * Extracts and organizes the elements in the tree structure of an XML element
 */
public class XMLAnalyser {

    /** root element of the XML */
    private Element root;

    /** HashMap of the Use Cases (model functions) in the Use Case Diagram */
    private Functions functions;

    /** HashMap of the Blocks (model components) in the Block Diagram */
    private Components comp;

    /** HashMap of the Requirements (model requirements) in the Requirement Diagram */
    private Requirements requirements;

    /** Contains all the relations between elements, once mapping is achieved */
    private FullModel fullModel;

    /**
     * Constructor that search for the elements of the SysML model, categorize them
     * connect them depending on their relations
     * 
     * @param root the root Element of the XML file
     */
    public XMLAnalyser(Element root) {

        this.root = root;

        this.functions = new Functions();
        this.comp = new Components();
        this.requirements = new Requirements();

        this.fullModel = new FullModel();

        // parse the file and select functions, components and requirements
        this.extractAll();
        this.fullModel.make();
        this.functions.make();
        this.comp.make();
        this.requirements.make();
    }

    /**
     * When parsing the XML file, classify elements depending on their nature : Use
     * Case Diagram elements Block Diagram elements Requirement Diagram elements
     */
    public void extractAll() {

        for (Element model : this.root.getChildren()) {

            // Search for Use Case Diagram
            if (model.getAttributeValue(TYPE).compareTo(AVATAR_ANALYSIS) == 0) {
                Element ucd_root = model.getChild(USE_CASE_DIAGRAM_PANEL);
                if (ucd_root != null) {
                    this.functions.add(ucd_root);
                    this.fullModel.addUcdPart(ucd_root);
                }

                // Search for Block Diagram
            } else if (model.getAttributeValue(TYPE).compareTo(AVATAR_DESIGN) == 0) {
                Element bd_root = model.getChild(AVATAR_BLOCK_DIAGRAM_PANEL);
                if (bd_root != null) {
                    this.comp.add(bd_root);
                    this.fullModel.addBdPart(bd_root);
                }

                // Search for Requirement Diagram
            } else if (model.getAttributeValue(TYPE).compareTo(AVATAR_REQUIREMENT) == 0) {
                Element req_root = model.getChild(AVATAR_RD_PANEL);
                if (req_root != null) {
                    this.requirements.add(req_root);
                    this.fullModel.addReqPart(req_root);
                }
            }

        }

    }

    /**
     * requirements getter
     * 
     * @return requirements of the model
     */
    public Requirements getRequirements() {
        return this.requirements;
    }

    /**
     * requirements setter
     * 
     * @param requirements requirements of the model
     */
    public void setRequirements(Requirements requirements) {
        this.requirements = requirements;
    }

    /**
     * root getter
     * 
     * @return root of the tree structure describing the XML
     */
    public Element getRoot() {
        return this.root;
    }

    /**
     * root setter
     * 
     * @param root root of the XML structure
     */
    public void setRoot(Element root) {
        this.root = root;
    }

    /**
     * functions getter
     * 
     * @return functions of the model
     */
    public Functions getFunctions() {
        return this.functions;
    }

    /**
     * functions setter
     * 
     * @param functions functions of the model
     */
    public void setFunctions(Functions functions) {
        this.functions = functions;
    }

    /**
     * components getter
     * 
     * @return components of the model
     */
    public Components getComp() {
        return this.comp;
    }

    /**
     * components setter
     * 
     * @param comp components of the model
     */
    public void setComp(Components comp) {
        this.comp = comp;
    }

    /**
     * fullmodel getter
     * 
     * @return fullmodel with all the elements of the model 
     */
    public FullModel getFullModel() {
        return this.fullModel;
    }

    /**
     * fullModel setter
     * 
     * @param fullModel whole model description
     */
    public void setFullModel(FullModel fullModel) {
        this.fullModel = fullModel;
    }


}
