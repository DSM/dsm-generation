package com.MB2DM.UserCommand;

import static com.MB2DM.Constants.Constants.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.WindowConstants;

import com.MB2DM.ExcelWriter.SheetWriter;
import com.MB2DM.MatrixMaker.*;
import com.MB2DM.XMLTreatment.*;
import com.MB2DM.gui.MyFrame;

import org.jdom2.Element;

/**
 * Calls a GUI Defines functions that can be used in the GUI
 */
public class UserCommand {

    private String message;

    /** Name of the file to parse */
    private String filename;

    /** XMLAnlayser containing the model information both extracted and organized */
    private XMLAnalyser analyser;

    /**
     * Contructor preparing the user interface: without GUI
     * 
     * @param filename file to be parsed
     */
    public UserCommand(String filename) {
        this.message = "Select a file";
        this.filename = filename;
        XMLParser parser = new XMLParser(this.filename);
        Element rootNode = parser.getRootNode();
        this.analyser = new XMLAnalyser(rootNode);
    }

    /**
     * Contructor preparing the user interface: with GUI
     * 
     * @param filename file to be parsed
     */
    public UserCommand() {
        this.message = "Select a file";
        this.filename = "";
        this.analyser = null;
    }

    /**
     * Launch a GUI
     * 
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws InterruptedException
     */
    public void userInterface() throws ClassNotFoundException, IOException, InterruptedException {

        // Enter an infinite loop, which can be broken by the user entering "EXIT"

        // Read the instruction from the user
        MyFrame frame = new MyFrame(this);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Creates the folders where CSV matrices are store

        File file = new File("design_matrices");
        if (!file.exists() && !file.isDirectory()){
            Path path = Paths.get("design_matrices");
            Files.createDirectory(path);
        }
        File mdm = new File("design_matrices/MDM");
        if (!mdm.exists() && !mdm.isDirectory()){
            Path path = Paths.get("design_matrices/MDM");
            Files.createDirectory(path);
        }
        File dsm = new File("design_matrices/DSM");
        if (!dsm.exists() && !dsm.isDirectory()){
            Path path = Paths.get("design_matrices/DSM");
            Files.createDirectory(path);
        }
        File dmm = new File("design_matrices/DMM");
        if (!dmm.exists() && !dmm.isDirectory()){
            Path path = Paths.get("design_matrices/DMM");
            Files.createDirectory(path);
        }

        // wait until no file has been selected
        while (this.filename.compareTo("") == 0) {
            Thread.sleep(1000);
        }
        
        // parse the selected file
        XMLParser parser = new XMLParser(this.filename);
        Element rootNode = parser.getRootNode();
        this.analyser = new XMLAnalyser(rootNode);
        System.out.println(this.filename);

        String previousFile = this.filename;

        this.message = "";

        while (!frame.getB()) {

            // detects change of file 
            if (previousFile.compareTo(this.filename) != 0) {
                parser = new XMLParser(this.filename);
                rootNode = parser.getRootNode();
                this.analyser = new XMLAnalyser(rootNode);
            }

            Thread.sleep(1000);
        }
    }

    /**
     * Write in files all the matrices that can be exctracted from the model
     */
    public void All() {

        DSMMaker dsm = new DSMMaker(this.analyser);
        dsm.makeBinary();

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.make();

        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.make();

        SheetWriter sh = new SheetWriter();

        try {
            sh.writeData(dmm);
            //sh.writeData(dsm);
            sh.writeData(mdm);
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }

    }

    /**
     * Write a Function Binary DSM
     */

    public void FuntionsBinaryDSM() {

        // analyses the model to create the DSM
        DSMMaker dsm = new DSMMaker(this.analyser);
        // build the matrix
        dsm.makeBinary();
        //writes the matrix in a file
        SheetWriter sh = new SheetWriter();
        try {

            sh.writeData(dsm.getBinaryUcdMatrix());

        } catch (IOException e) {
            System.out.println(e);
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Components Binary DSM
     */
    public void ComponentsBinaryDSM() {

        DSMMaker dsm = new DSMMaker(this.analyser);
        dsm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {

            sh.writeData(dsm.getBinaryBdMatrix());

        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Requirements Binary DSM
     */
    public void RequirementsBinaryDSM() {

        DSMMaker dsm = new DSMMaker(this.analyser);
        dsm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {

            sh.writeData(dsm.getBinaryReqMatrix());

        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Functions Directed DSM
     */
    public void FuntionsDirectedDSM() {

        DSMMaker dsm = new DSMMaker(this.analyser);
        dsm.makeDirected();

        try {
            SheetWriter sh = new SheetWriter();
            sh.writeData(dsm.getDirectedUcdMatrix());
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Components Directed DSM
     */
    public void ComponentsDirectedDSM() {

        DSMMaker dsm = new DSMMaker(this.analyser);
        dsm.makeDirected();

        try {
            SheetWriter sh = new SheetWriter();
            sh.writeData(dsm.getDirectedBdMatrix());
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Requirements Directed DSM
     */
    public void RequirementsDirectedDSM() {

        DSMMaker dsm = new DSMMaker(this.analyser);
        dsm.makeDirected();

        try {
            SheetWriter sh = new SheetWriter();
            sh.writeData(dsm.getDirectedReqMatrix());
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Binary MDM
     */
    public void BinaryMDM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {
            sh.writeData(mdm.getBinaryMDM());
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Directed MDM
     */
    public void DirectedMDM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeDirected();

        SheetWriter sh = new SheetWriter();
        try {
            sh.writeData(mdm.getDirectedMDM());
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Functions / Components Directed DMM
     */
    public void Functions_ComponentsDirectedDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeDirected();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeDirected();

        SheetWriter sh = new SheetWriter();
        try {
            String name = FUNCTION_PART + "_" + COMPONENT_PART;
            sh.writeData(dmm.getDirectedMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Components / Functions Binary DMM
     */
    public void Functions_ComponentsBinaryDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeBinary();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {
            String name = FUNCTION_PART + "_" + COMPONENT_PART;
            sh.writeData(dmm.getBinaryMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Components / Functions Directed DMM
     */
    public void Components_FunctionsDirectedDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeDirected();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeDirected();

        SheetWriter sh = new SheetWriter();
        try {
            String name = COMPONENT_PART + "_" + FUNCTION_PART;
            sh.writeData(dmm.getDirectedMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Components / Functions Binary DMM
     */
    public void Components_FunctionsBinaryDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeBinary();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {
            String name = COMPONENT_PART + "_" + FUNCTION_PART;
            sh.writeData(dmm.getBinaryMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Components / Requirements Directed DMM
     */
    public void Components_RequirementsDirectedDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeDirected();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeDirected();

        SheetWriter sh = new SheetWriter();
        try {
            String name = COMPONENT_PART + "_" + REQUIREMENT_PART;
            sh.writeData(dmm.getDirectedMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Components / Requirements Binary DMM
     */
    public void Components_RequirementsBinaryDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeBinary();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {
            String name = COMPONENT_PART + "_" + REQUIREMENT_PART;
            sh.writeData(dmm.getBinaryMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Requirements / Components Directed DMM
     */
    public void Requirements_ComponentsDirectedDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeDirected();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeDirected();

        SheetWriter sh = new SheetWriter();
        try {
            String name = REQUIREMENT_PART + "_" + COMPONENT_PART;
            sh.writeData(dmm.getDirectedMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Requirements / Components Binary DMM
     */
    public void Requirements_ComponentsBinaryDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeBinary();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {
            String name = REQUIREMENT_PART + "_" + COMPONENT_PART;
            sh.writeData(dmm.getBinaryMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Functions / Requirements Directed DMM
     */
    public void Functions_RequirementsDirectedDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeDirected();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeDirected();

        SheetWriter sh = new SheetWriter();
        try {
            String name = FUNCTION_PART + "_" + REQUIREMENT_PART;
            sh.writeData(dmm.getDirectedMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Functions / Requirements Binary DMM
     */
    public void Functions_RequirementsBinaryDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeBinary();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {
            String name = FUNCTION_PART + "_" + REQUIREMENT_PART;
            sh.writeData(dmm.getBinaryMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Requirements / Functions Directed DMM
     */
    public void Requirements_FunctionsDirectedDMM() {

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeDirected();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeDirected();

        SheetWriter sh = new SheetWriter();
        try {
            String name = REQUIREMENT_PART + "_" + FUNCTION_PART;
            sh.writeData(dmm.getDirectedMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    /**
     * Write a Requirements / Functions Binary DMM
     * @return 
     */
    public void Requirements_FunctionsBinaryDMM(){

        MDMMaker mdm = new MDMMaker(this.analyser);
        mdm.makeBinary();
        DMMMaker dmm = new DMMMaker(this.analyser, mdm);
        dmm.makeBinary();

        SheetWriter sh = new SheetWriter();
        try {
            String name = REQUIREMENT_PART + "_" + FUNCTION_PART;
            sh.writeData(dmm.getBinaryMatrixList().get(name));
        } catch (IOException e) {
            this.message = "Make sure no Excel file is currently open";
        }
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public XMLAnalyser getAnalyser() {
        return this.analyser;
    }

    public void setAnalyser(XMLAnalyser analyser) {
        this.analyser = analyser;
    }

}
