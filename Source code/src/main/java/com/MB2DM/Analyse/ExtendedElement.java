package com.MB2DM.Analyse;

import java.util.ArrayList;

import org.jdom2.Element;
import static com.MB2DM.Constants.Constants.*;
/**
 * a Wrapper containing the Element class of JDOM
 * it aims at gathering different instances of a same element
 */
public class ExtendedElement {

    /** instances of a same element */
    private ArrayList<Element> element;

    /** element name */
    private String name;

    /** element category */
    private String category;

    /**
     * Constructor
     * 
     * @param element 
     * @param category
     */
    public ExtendedElement(ArrayList<Element> element, String category) {
        this.element = element;

        if (element.size() > 0) {
            this.name = element.get(0).getChild(INFOPARAM).getAttributeValue(VALUE);
        } else {
            this.name = "SET NAME";
        }

        this.category = category;
    }

    /**
     * Constructor
     * 
     * @param category
     */
    public ExtendedElement(String category) {
        this.element = new ArrayList<Element>();

        this.name = "SET NAME";

        this.category = category;
    }

    /**
     * Constructor
     * 
     * @param element
     * @param category
     */
    public ExtendedElement(Element element, String category) {
        this.element = new ArrayList<Element>();
        this.addElement(element);

        this.name = element.getChild(INFOPARAM).getAttributeValue(VALUE);

        this.category = category;
    }

    /**
     * Constructor
     * 
     * @param element
     * @param name
     * @param category
     */
    public ExtendedElement(Element element, String name, String category) {
        this.element = new ArrayList<Element>();
        this.addElement(element);

        this.name = name;

        this.category = category;
    }

    /**
     * add an element to an extended element
     * @param element the element to be added
     */
    public void addElement(Element element){
        this.element.add(element);
    }

    public ArrayList<Element> getElement() {
        return this.element;
    }

    public void setElement(ArrayList<Element> element) {
        this.element = element;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
