package com.MB2DM.Analyse.FullModel;

import static com.MB2DM.Constants.Constants.*;

import java.util.ArrayList;
import java.util.HashMap;

import com.MB2DM.Analyse.ExtendedElement;
import com.MB2DM.Analyse.Relations.Connection;
import com.MB2DM.Analyse.Relations.Relation;

import org.jdom2.Element;

/**
 * Contains all the system model data
 */
public class FullModel {

    final String TOTAL = "Total";

    private HashMap<String, HashMap<String, ExtendedElement>> fullModel;

    private ArrayList<ExtendedElement> allElements;

    /** List of CONNECTOR elements */
    private ArrayList<Element> connectors;

    /** List of Relations that can be built from raw data */
    private ArrayList<Relation> relations;

    /** Number of elements in Functions, Components and Requirements DSM */
    private HashMap<String, Integer> diagramsSize;

    /**
     * Constructor to parse and analyse the whole model stored in the XML
     */
    public FullModel() {

        this.fullModel = new HashMap<String, HashMap<String, ExtendedElement>>();
        this.fullModel.put(FUNCTION_PART, new HashMap<String, ExtendedElement>());
        this.fullModel.put(COMPONENT_PART, new HashMap<String, ExtendedElement>());
        this.fullModel.put(REQUIREMENT_PART, new HashMap<String, ExtendedElement>());
        this.fullModel.put(ACTOR, new HashMap<String, ExtendedElement>());
        this.fullModel.put(SUBCOMPONENT, new HashMap<String, ExtendedElement>());
        this.fullModel.put(PROPERTY, new HashMap<String, ExtendedElement>());

        this.allElements = new ArrayList<ExtendedElement>();

        this.connectors = new ArrayList<Element>();

        this.relations = new ArrayList<Relation>();

        this.diagramsSize = new HashMap<String, Integer>();
        for (String category : this.fullModel.keySet()) {
            this.diagramsSize.put(category, 0);
        }
        this.diagramsSize.put(TOTAL, 0);

    }

    public void make() {
        this.shapeElements();
        this.connect();
    }

    public void addElement(Element element, String category) {
        String name = element.getChild(INFOPARAM).getAttributeValue(VALUE);
        if (this.fullModel.get(category).containsKey(name)) {
            this.fullModel.get(category).get(name).addElement(element);
        } else {
            ExtendedElement elements = new ExtendedElement(element, name, category);
            this.fullModel.get(category).put(name, elements);
            this.diagramsSize.put(category, this.diagramsSize.get(category) + 1);
            if (category.compareTo(FUNCTION_PART) == 0 || category.compareTo(COMPONENT_PART) == 0
                    || category.compareTo(REQUIREMENT_PART) == 0) {
                this.diagramsSize.put(TOTAL, this.diagramsSize.get(TOTAL) + 1);
            }
        }

    }

    /**
     * Add elements of a Use Case Diagram
     * 
     * @param ucd_root Use Case Diagram main root in the XML
     */
    public void addUcdPart(Element ucd_root) {

        for (Element element : ucd_root.getChildren()) {

            if (element.getName().compareTo(COMPONENT) == 0 && (element.getAttributeValue(TYPE).compareTo(ACTOR) == 0
                    || element.getAttributeValue(TYPE).compareTo(BOX_ACTOR) == 0)) {

                this.addElement(element, ACTOR);

            } else if (element.getName().compareTo(COMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(USE_CASE) == 0) {

                this.addElement(element, FUNCTION_PART);

            } else if (element.getName().compareTo(CONNECTOR) == 0) {

                this.connectors.add(element);
            }
        }
    }

    /**
     * Add elements of a Block Diagram
     * 
     * @param bd_root Block Diagram main root in the XML
     */
    public void addBdPart(Element bd_root) {

        for (Element element : bd_root.getChildren()) {

            if (element.getName().compareTo(COMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(BLOCK_CRYPTO) == 0) {

                this.addElement(element, COMPONENT_PART);

            } else if (element.getName().compareTo(SUBCOMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(BLOCK_CRYPTO) == 0) {

                this.addElement(element, COMPONENT_PART);
                this.addElement(element, SUBCOMPONENT);

            } else if (element.getName().compareTo(CONNECTOR) == 0) {
                this.connectors.add(element);
            }
        }
    }

    /**
     * Add elements of a Requirement Diagram
     * 
     * @param req_root Requirement Diagram main root in the XML
     */
    public void addReqPart(Element req_root) {

        for (Element element : req_root.getChildren()) {
            if (element.getName().compareTo(COMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(REQUIREMENT) == 0) {

                this.addElement(element, REQUIREMENT_PART);

            } else if (element.getName().compareTo(COMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(PROPERTY) == 0) {

                this.addElement(element, PROPERTY);

            } else if (element.getName().compareTo(CONNECTOR) == 0) {
                this.connectors.add(element);
            }
        }

    }

    /**
     * Fills lists that are necessary to create Relation objects between elements
     */
    public void shapeElements() {

        for (ExtendedElement element : this.fullModel.get(FUNCTION_PART).values()) {
            this.allElements.add(element);
        }
        for (ExtendedElement element : this.fullModel.get(COMPONENT_PART).values()) {
            this.allElements.add(element);
        }
        for (ExtendedElement element : this.fullModel.get(REQUIREMENT_PART).values()) {
            this.allElements.add(element);
        }
    }

    /**
     * Creates Relation objects between the elements
     */
    public void connect() {

        ArrayList<ExtendedElement> allElementsWithActors = new ArrayList<ExtendedElement>();
        allElementsWithActors.addAll(fullModel.get(FUNCTION_PART).values());
        allElementsWithActors.addAll(fullModel.get(ACTOR).values());
        allElementsWithActors.addAll(fullModel.get(COMPONENT_PART).values());
        allElementsWithActors.addAll(fullModel.get(REQUIREMENT_PART).values());

        for (Element connector : this.connectors) {

            String id_ConnectingPoint_1 = connector.getChild(P1).getAttributeValue(ID);
            String id_ConnectingPoint_2 = connector.getChild(P2).getAttributeValue(ID);

            for (ExtendedElement elements1 : allElementsWithActors) {

                for (Element P1 : elements1.getElement()) {
                    ArrayList<String> L1 = new ArrayList<>();

                    for (Element connectingPoint1 : P1.getChildren(TG_CONNECTING_POINT)) {
                        L1.add(connectingPoint1.getAttributeValue(ID));
                    }

                    for (ExtendedElement elements2 : allElementsWithActors) {

                        for (Element P2 : elements2.getElement()) {
                            ArrayList<String> L2 = new ArrayList<>();

                            for (Element connectingPoint1 : P2.getChildren(TG_CONNECTING_POINT)) {
                                L2.add(connectingPoint1.getAttributeValue(ID));
                            }

                            if (L1.contains(id_ConnectingPoint_1) && L2.contains(id_ConnectingPoint_2)) {

                                if (P1.getAttributeValue(TYPE).compareTo(ACTOR) == 0
                                        && P2.getAttributeValue(TYPE).compareTo(ACTOR) != 0
                                        || P1.getAttributeValue(TYPE).compareTo(ACTOR) == 0
                                                && P2.getAttributeValue(TYPE).compareTo(BOX_ACTOR) != 0
                                        || P1.getAttributeValue(TYPE).compareTo(BOX_ACTOR) == 0
                                                && P2.getAttributeValue(TYPE).compareTo(ACTOR) != 0
                                        || P1.getAttributeValue(TYPE).compareTo(BOX_ACTOR) == 0
                                                && P2.getAttributeValue(TYPE).compareTo(BOX_ACTOR) != 0) {

                                    for (ExtendedElement blocks : this.fullModel.get(COMPONENT_PART).values()) {
                                        if (blocks.getName().compareTo(elements1.getName()) == 0) {
                                            if (connector.getAttributeValue(TYPE).compareTo(GENERALIZATION) == 0) {
                                                this.relations.add(
                                                        new Relation(blocks, elements2, new Connection(connector)));
                                            } else if (connector.getAttributeValue(TYPE).compareTo(ASSOCIATION) == 0) {
                                                this.relations.add(
                                                        new Relation(blocks, elements2, new Connection(connector)));
                                                this.relations.add(
                                                        new Relation(elements2, blocks, new Connection(connector)));
                                            }
                                        }
                                    }
                                }

                                else if (P1.getAttributeValue(TYPE).compareTo(ACTOR) != 0
                                        && P2.getAttributeValue(TYPE).compareTo(ACTOR) == 0
                                        || P1.getAttributeValue(TYPE).compareTo(ACTOR) != 0
                                                && P2.getAttributeValue(TYPE).compareTo(BOX_ACTOR) == 0
                                        || P1.getAttributeValue(TYPE).compareTo(BOX_ACTOR) != 0
                                                && P2.getAttributeValue(TYPE).compareTo(ACTOR) == 0
                                        || P1.getAttributeValue(TYPE).compareTo(BOX_ACTOR) != 0
                                                && P2.getAttributeValue(TYPE).compareTo(BOX_ACTOR) == 0) {

                                    for (ExtendedElement blocks : this.fullModel.get(COMPONENT_PART).values()) {
                                        if (blocks.getName().compareTo(elements2.getName()) == 0) {
                                            if (connector.getAttributeValue(TYPE).compareTo(GENERALIZATION) == 0) {
                                                this.relations.add(
                                                        new Relation(elements1, blocks, new Connection(connector)));
                                            } else if (connector.getAttributeValue(TYPE).compareTo(ASSOCIATION) == 0) {
                                                this.relations.add(
                                                        new Relation(elements1, blocks, new Connection(connector)));
                                                this.relations.add(
                                                        new Relation(blocks, elements1, new Connection(connector)));
                                            }
                                        }
                                    }
                                }

                                else {
                                    if (connector.getAttributeValue(TYPE).compareTo(PORT_CONNECTION) == 0) {
                                        this.relations.add(new Relation(elements1, elements2,
                                                new Connection(connector, PORT_CONNECTION)));
                                        // this.relations.add(new Relation(elements2, elements1,
                                        // new Connection(connector, PORT_CONNECTION)));
                                    } else {
                                        this.relations
                                                .add(new Relation(elements1, elements2, new Connection(connector)));
                                    }
                                }
                            }
                        }

                    }

                }

            }

        }

        for (ExtendedElement subcomponents : this.fullModel.get(SUBCOMPONENT).values()) {
            ArrayList<String> fathersNames = new ArrayList<String>();

            for (Element P1 : subcomponents.getElement()) {
                ArrayList<String> L1 = new ArrayList<>();

                for (Element father : P1.getChildren(FATHER)) {
                    L1.add(father.getAttributeValue(ID));
                }

                for (ExtendedElement elements : allElements) {
                    for (Element P2 : elements.getElement()) {

                        if (L1.contains(P2.getAttributeValue(ID)) && !fathersNames.contains(elements.getName())) {
                            fathersNames.add(elements.getName());

                            for (ExtendedElement Extended : allElements) {
                                if (Extended.getName().compareTo(subcomponents.getName()) == 0) {

                                    Boolean existingRelation = false;
                                    for (Relation relation : this.relations) {

                                        if (relation.getElement1Name().compareTo(Extended.getName()) == 0
                                                && relation.getElement2Name().compareTo(elements.getName()) == 0) {

                                            relation.addE1Output();
                                            existingRelation = true;
                                        }
                                    }
                                    if (!existingRelation) {

                                        this.relations
                                                .add(new Relation(Extended, elements, new Connection("child of")));
                                    }

                                }
                            }

                        }

                    }
                }

            }

        }
    }

    public void relationsDisplay() {
        for (Relation relation : this.relations) {
            System.out.println(relation.getElement1Category() + " - " + relation.getElement1Name() + "   :"
                    + relation.getE1outputs() + ": " + relation.getConnection().getValue() + "   :"
                    + relation.getE2outputs() + ":   " + relation.getElement2Category() + " - "
                    + relation.getElement2Name());
        }
    }

    public String getTOTAL() {
        return this.TOTAL;
    }

    public HashMap<String, HashMap<String, ExtendedElement>> getFullModel() {
        return this.fullModel;
    }

    public void setFullModel(HashMap<String, HashMap<String, ExtendedElement>> fullModel) {
        this.fullModel = fullModel;
    }

    public ArrayList<ExtendedElement> getAllElements() {
        return this.allElements;
    }

    public void setAllElements(ArrayList<ExtendedElement> allElements) {
        this.allElements = allElements;
    }

    public ArrayList<Element> getConnectors() {
        return this.connectors;
    }

    public void setConnectors(ArrayList<Element> connectors) {
        this.connectors = connectors;
    }

    public ArrayList<Relation> getRelations() {
        return this.relations;
    }

    public void setRelations(ArrayList<Relation> relations) {
        this.relations = relations;
    }

    public HashMap<String, Integer> getDiagramsSize() {
        return this.diagramsSize;
    }

    public void setDiagramsSize(HashMap<String, Integer> diagramsSize) {
        this.diagramsSize = diagramsSize;
    }

}
