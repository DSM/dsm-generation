package com.MB2DM.Analyse.Relations;

import com.MB2DM.Analyse.ExtendedElement;

import org.jdom2.Element;

import static com.MB2DM.Constants.Constants.*;

import java.util.ArrayList;

/**
 * Defines a dependency between two elements
 */
public class Relation {

    /** First member of the relation */
    private ExtendedElement element1;

    /** First member name */
    private String element1Name;

    /** First member category */
    private String element1Category;

    /** Second member of the relation */
    private ExtendedElement element2;

    /** Second member name */
    private String element2Name;

    /** Second member category */
    private String element2Category;

    /** Number of outputs for the first element */
    private Integer E1outputs;

    /** Number of outputs for the second elements */
    private Integer E2outputs;

    /** Defines the type of dependency between element1 and element2 */
    private Connection connection;

    /**
     * Constructor of a Relation that represents a relation in the XML file (without
     * information)
     * 
     * @param element1     first member of a connection
     * @param element1Name
     * @param element2     second member of a connection
     * @param element2Name
     * @param E1outputs
     * @param E2outputs
     * @param connection   Connection object between element1 and element2
     */

    public Relation(ExtendedElement element1, ExtendedElement element2, Connection connection) {
        this.element1 = element1;
        this.element1Name = element1.getName();
        this.element1Category = element1.getCategory();
        this.element2 = element2;
        this.element2Name = element2.getName();
        this.element2Category = element2.getCategory();
        this.connection = connection;

        this.E1outputs = 0;
        this.E2outputs = 0;

        if (connection.getValue().compareTo(PORT_CONNECTION) == 0) {

            // counts the number of outputs for the firsts element
            ArrayList<String> E1signals = new ArrayList<String>();
            for (Element signal : element1.getElement().get(0).getChild(EXTRAPARAM).getChildren(SIGNAL)) {
                E1signals.add(signal.getAttributeValue(VALUE));
            }

            // counts the number of outputs for the second element
            ArrayList<String> E2signals = new ArrayList<String>();
            for (Element signal : element2.getElement().get(0).getChild(EXTRAPARAM).getChildren(SIGNAL)) {
                E2signals.add(signal.getAttributeValue(VALUE));
            }

            for (Element output : connection.getConnector().getChild(EXTRAPARAM).getChildren(OSO)) {
                if (E1signals.contains(output.getAttributeValue(VALUE))) {
                    this.E1outputs++;
                }
                if (E2signals.contains(output.getAttributeValue(VALUE))) {
                    this.E2outputs++;
                }
            }
            for (Element output : connection.getConnector().getChild(EXTRAPARAM).getChildren(OSD)) {

                if (E1signals.contains(output.getAttributeValue(VALUE))) {
                    this.E1outputs++;
                }
                if (E1signals.contains(output.getAttributeValue(VALUE))) {
                    this.E2outputs++;
                }
            }

        } else {
            this.E1outputs = 1;
            this.E2outputs = 1;
        }
    }

    /**
     * Constructor with information
     * 
     * @param element1
     * @param element2
     * @param category2
     * @param connection
     */
    public Relation(ExtendedElement element1, ExtendedElement element2, String category2, Connection connection) {
        this.element1 = element1;
        this.element1Name = element1.getName();
        this.element1Category = element1.getCategory();
        this.element2 = element2;
        this.element2Name = element2.getName();
        this.element2Category = category2;
        this.connection = connection;
        this.E1outputs = 1;
        this.E2outputs = 1;
    }

    /**
     * Add one output for the first element
     */
    public void addE1Output() {
        this.E1outputs++;
    }

    /**
     * Constructor with information
     * 
     * @param element1
     * @param category1
     * @param element2
     * @param connection
     */
    public Relation(ExtendedElement element1, String category1, ExtendedElement element2, Connection connection) {
        this.element1 = element1;
        this.element1Name = element1.getName();
        this.element1Category = category1;
        this.element2 = element2;
        this.element2Name = element2.getName();
        this.element2Category = element2.getCategory();
        this.connection = connection;
        this.E1outputs = 1;
        this.E2outputs = 1;
    }

    public ExtendedElement getElement1() {
        return this.element1;
    }

    public void setElement1(ExtendedElement element1) {
        this.element1 = element1;
    }

    public String getElement1Name() {
        return this.element1Name;
    }

    public void setElement1Name(String element1Name) {
        this.element1Name = element1Name;
    }

    public String getElement1Category() {
        return this.element1Category;
    }

    public void setElement1Category(String element1Category) {
        this.element1Category = element1Category;
    }

    public ExtendedElement getElement2() {
        return this.element2;
    }

    public void setElement2(ExtendedElement element2) {
        this.element2 = element2;
    }

    public String getElement2Name() {
        return this.element2Name;
    }

    public void setElement2Name(String element2Name) {
        this.element2Name = element2Name;
    }

    public String getElement2Category() {
        return this.element2Category;
    }

    public void setElement2Category(String element2Category) {
        this.element2Category = element2Category;
    }

    public Integer getE1outputs() {
        return this.E1outputs;
    }

    public void setE1outputs(Integer E1outputs) {
        this.E1outputs = E1outputs;
    }

    public Integer getE2outputs() {
        return this.E2outputs;
    }

    public void setE2outputs(Integer E2outputs) {
        this.E2outputs = E2outputs;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

}
