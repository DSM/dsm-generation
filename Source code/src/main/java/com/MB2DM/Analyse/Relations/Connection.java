package com.MB2DM.Analyse.Relations;

import static com.MB2DM.Constants.Constants.*;

import org.jdom2.Element;

/** Defines the type of a dependency between system elements */
public class Connection {

    /** A manual characterization of the dependency */
    private String value;

    /** The XML node defining the dependency */
    private Element connector;

    /**
     * Constructor of a Connection that can be described with attributes values in
     * the XML
     * 
     * @param connector CONNECTION Element in the XML
     */
    public Connection(Element connector) {
        this.connector = connector;
        this.value = connector.getChild(INFOPARAM).getAttributeValue(VALUE);
    }

    /**
     * Constructor of a Connection that can be described with attributes values in
     * the XML
     * 
     * @param connector CONNECTION Element in the XML
     * @param value value of the connection to be set manually
     */
    public Connection(Element connector, String value) {
        this.connector = connector;
        this.value = value;
    }

    /**
     * Constructor of a Connection that cannot be described with attributes values
     * in the XML
     * 
     * @param value name given to the connection
     */
    public Connection(String value) {
        this.value = value;
        this.connector = null;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Element getConnector() {
        return this.connector;
    }

    public void setConnector(Element connector) {
        this.connector = connector;
    }

}
