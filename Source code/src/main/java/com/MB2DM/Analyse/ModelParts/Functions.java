package com.MB2DM.Analyse.ModelParts;

import static com.MB2DM.Constants.Constants.*;

import java.util.ArrayList;
import java.util.HashMap;

import com.MB2DM.Analyse.ExtendedElement;
import com.MB2DM.Analyse.Relations.Connection;
import com.MB2DM.Analyse.Relations.Relation;

import org.jdom2.Element;

/**
 * Maps elements of a Use Case Diagram
 */
public class Functions extends Diagram {

    private HashMap<String, HashMap<String, ExtendedElement>> functions;

    /**
     * Constructor
     * 
     */
    public Functions() {

        super();

        this.functions = new HashMap<String, HashMap<String, ExtendedElement>>();
        this.functions.put(USE_CASE, new HashMap<String, ExtendedElement>());
    }

    /**
     * Creates relations between requirements
     */
    public void make() {
        this.shapeElements();
        this.connect();
    }

    /**
     * Add a function to the previously found functions after comparing it to
     * avoid instances of same functions
     * 
     * @param element
     * @param category
     */
    public void addElement(Element element, String category) {
        String name = element.getChild(INFOPARAM).getAttributeValue(VALUE);
        if (this.functions.get(category).containsKey(name)) {
            this.functions.get(category).get(name).addElement(element);
        } else {
            ExtendedElement elements = new ExtendedElement(element, name, category);
            this.functions.get(category).put(name, elements);
        }
    }

    /**
     * Find all the functions of the model
     * @param ucd_root
     */
    public void add(Element ucd_root) {

        for (Element element : ucd_root.getChildren()) {

            if (element.getName().compareTo(COMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(USE_CASE) == 0) {

                this.addElement(element, USE_CASE);

            } else if (element.getName().compareTo(CONNECTOR) == 0) {

                this.getConnectors().add(element);
            }
        }
    }

    /**
     * Fills lists that are necessary to create Relation objects between elements
     */
    public void shapeElements() {

        for (ExtendedElement element : this.functions.get(USE_CASE).values()) {
            this.getAllelements().add(element);
        }
    }

    /**
     * Creates Relation objects to represents the relations in the model
     */
    public void connect() {

        ArrayList<ExtendedElement> use_cases = this.getAllelements();

        for (Element connector : this.getConnectors()) {

            String id_ConnectingPoint_1 = connector.getChild(P1).getAttributeValue(ID);
            String id_ConnectingPoint_2 = connector.getChild(P2).getAttributeValue(ID);

            for (ExtendedElement elements1 : use_cases) {
                for (Element P1 : elements1.getElement()) {
                    ArrayList<String> L1 = new ArrayList<>();

                    for (Element connectingPoint1 : P1.getChildren(TG_CONNECTING_POINT)) {
                        L1.add(connectingPoint1.getAttributeValue(ID));
                    }

                    for (ExtendedElement elements2 : use_cases) {

                        for (Element P2 : elements2.getElement()) {
                            ArrayList<String> L2 = new ArrayList<>();

                            for (Element connectingPoint1 : P2.getChildren(TG_CONNECTING_POINT)) {
                                L2.add(connectingPoint1.getAttributeValue(ID));
                            }

                            if (L1.contains(id_ConnectingPoint_1) && L2.contains(id_ConnectingPoint_2)) {
                                this.getRelations().add(new Relation(elements1, elements2, new Connection(connector)));
                            }
                        }
                    }

                }
            }

        }
    }

    public HashMap<String, HashMap<String, ExtendedElement>> getFunctions() {
        return this.functions;
    }

    public void setFunctions(HashMap<String, HashMap<String, ExtendedElement>> functions) {
        this.functions = functions;
    }

}
