package com.MB2DM.Analyse.ModelParts;

import static com.MB2DM.Constants.Constants.*;

import java.util.ArrayList;
import java.util.HashMap;

import com.MB2DM.Analyse.ExtendedElement;
import com.MB2DM.Analyse.Relations.Connection;
import com.MB2DM.Analyse.Relations.Relation;

import org.jdom2.Element;

/**
 * Maps elements of a Block Diagram
 */
public class Components extends Diagram {

    private HashMap<String, HashMap<String, ExtendedElement>> comp;

    /**
     * Constructor
     * 
     * @param root node that must be analysed
     */
    public Components() {

        super();
        this.comp = new HashMap<String, HashMap<String, ExtendedElement>>();
        this.comp.put(COMPONENT_PART, new HashMap<String, ExtendedElement>());
        this.comp.put(SUBCOMPONENT, new HashMap<String, ExtendedElement>());

    }

    public void make() {
        this.shapeElements();
        this.connect();
    }

    /**
     * Classify XML elements between components, subcomponents and connectors
     */
    /*
     * public void classify() {
     * 
     * for (Element element : this.getAllelements()) { if
     * (element.getName().compareTo(COMPONENT) == 0) {
     * this.getComponents().add(element); } else if
     * (element.getName().compareTo(SUBCOMPONENT) == 0) {
     * this.getSubcomponents().add(element); } else if
     * (element.getName().compareTo(CONNECTOR) == 0) {
     * this.getConnectors().add(element); } }
     * this.getAllcomponents().addAll(this.getComponents());
     * this.getAllcomponents().addAll(this.getSubcomponents());
     * 
     * for (Element element : this.getAllcomponents()) { if
     * (element.getAttributeValue(TYPE).compareTo(BLOCK_CRYPTO) == 0) {
     * this.blocksonly.add(element); } } }
     */

    public void addElement(Element element, String category) {
        String name = element.getChild(INFOPARAM).getAttributeValue(VALUE);
        if (this.comp.get(category).containsKey(name)) {
            this.comp.get(category).get(name).addElement(element);
        } else {
            ExtendedElement elements = new ExtendedElement(element, name, category);
            this.comp.get(category).put(name, elements);
        }
    }

    public void add(Element bd_root) {

        for (Element element : bd_root.getChildren()) {
            /*
             * if (element.getName().compareTo(COMPONENT) == 0 &&
             * (element.getAttributeValue(TYPE).compareTo(ACTOR) == 0 ||
             * element.getAttributeValue(TYPE).compareTo(BOX_ACTOR) == 0)) {
             * 
             * this.addElement(element, ACTOR); this.getAllelements().add(element);
             */
            if (element.getName().compareTo(COMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(BLOCK_CRYPTO) == 0) {

                this.addElement(element, COMPONENT_PART);

            } else if (element.getName().compareTo(SUBCOMPONENT) == 0
                    && element.getAttributeValue(TYPE).compareTo(BLOCK_CRYPTO) == 0) {

                this.addElement(element, COMPONENT_PART);
                this.addElement(element, SUBCOMPONENT);

            } else if (element.getName().compareTo(CONNECTOR) == 0) {

                this.getConnectors().add(element);
            }
        }
    }

    public void shapeElements() {

        for (ExtendedElement element : this.comp.get(COMPONENT_PART).values()) {
            this.getAllelements().add(element);
        }
        for (ExtendedElement element : this.comp.get(SUBCOMPONENT).values()) {
            this.getSubcomponents().add(element);
        }
    }

    /**
     * Creates Relation objects to represents the relations in the model
     */
    public void connect() {

        for (Element connector : this.getConnectors()) {

            String id_ConnectingPoint_1 = connector.getChild(P1).getAttributeValue(ID);
            String id_ConnectingPoint_2 = connector.getChild(P2).getAttributeValue(ID);

            for (ExtendedElement elements1 : this.getAllelements()) {
                for (Element P1 : elements1.getElement()) {
                    ArrayList<String> L1 = new ArrayList<>();

                    for (Element connectingPoint1 : P1.getChildren(TG_CONNECTING_POINT)) {
                        L1.add(connectingPoint1.getAttributeValue(ID));
                    }

                    for (ExtendedElement elements2 : this.getAllelements()) {

                        for (Element P2 : elements2.getElement()) {
                            ArrayList<String> L2 = new ArrayList<>();

                            for (Element connectingPoint1 : P2.getChildren(TG_CONNECTING_POINT)) {
                                L2.add(connectingPoint1.getAttributeValue(ID));
                            }

                            if (L1.contains(id_ConnectingPoint_1) && L2.contains(id_ConnectingPoint_2)) {
                                if (connector.getAttributeValue(TYPE).compareTo(PORT_CONNECTION) == 0) {
                                    this.getRelations().add(new Relation(elements1, elements2,
                                            new Connection(connector, PORT_CONNECTION)));
                                    // this.relations.add(new Relation(elements2, elements1,
                                    // new Connection(connector, PORT_CONNECTION)));
                                } else {
                                    this.getRelations()
                                            .add(new Relation(elements1, elements2, new Connection(connector)));
                                }
                            }
                        }
                    }

                }
            }

        }

        for (ExtendedElement subcomponents : this.getSubcomponents()) {
            ArrayList<String> fathersNames = new ArrayList<String>();

            for (Element P1 : subcomponents.getElement()) {
                ArrayList<String> L1 = new ArrayList<>();

                for (Element father : P1.getChildren(FATHER)) {
                    L1.add(father.getAttributeValue(ID));
                }

                for (ExtendedElement elements : this.getAllelements()) {
                    for (Element P2 : elements.getElement()) {

                        if (L1.contains(P2.getAttributeValue(ID)) && !fathersNames.contains(elements.getName())) {
                            fathersNames.add(elements.getName());

                            for (ExtendedElement Extended : this.getAllelements()) {
                                if (Extended.getName().compareTo(subcomponents.getName()) == 0) {

                                    Boolean existingRelation = false;
                                    for (Relation relation : this.getRelations()) {

                                        if (relation.getElement1Name().compareTo(Extended.getName()) == 0
                                                && relation.getElement2Name().compareTo(elements.getName()) == 0) {

                                            relation.addE1Output();
                                            existingRelation = true;
                                        }
                                    }
                                    if (!existingRelation) {

                                        this.getRelations()
                                                .add(new Relation(Extended, elements, new Connection("child of")));
                                    }

                                }
                            }

                        }

                    }
                }

            }

        }

    }

    public HashMap<String, HashMap<String, ExtendedElement>> getComp() {
        return this.comp;
    }

    public void setComp(HashMap<String, HashMap<String, ExtendedElement>> comp) {
        this.comp = comp;
    }

}
