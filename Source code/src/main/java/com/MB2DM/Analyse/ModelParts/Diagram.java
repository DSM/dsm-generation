package com.MB2DM.Analyse.ModelParts;

import java.util.ArrayList;

import com.MB2DM.Analyse.ExtendedElement;
import com.MB2DM.Analyse.Relations.Relation;

import org.jdom2.Element;

/** Generalization of a Diagram representation in the XML */
public class Diagram {

    /** List of all the elements of the parsed node */
    private ArrayList<ExtendedElement> allelements;

    /** List of all the COMPONENT elements */
    private ArrayList<ExtendedElement> components;

    /** List of all the SUBCOMPONENT elements */
    private ArrayList<ExtendedElement> subcomponents;

    /** List of both COMPONENT and SUBCOMPONENT elements */
    private ArrayList<ExtendedElement> allcomponents;

    /** List of all the CONNECTOR elements */
    private ArrayList<Element> connectors;

    /** List o all the Relations that can be built from the raw data */
    private ArrayList<Relation> relations;

    /**
     * Constructor
     * 
     */
    public Diagram() {

        this.allelements = new ArrayList<ExtendedElement>();
        this.components = new ArrayList<ExtendedElement>();
        this.subcomponents = new ArrayList<ExtendedElement>();
        this.allcomponents = new ArrayList<ExtendedElement>();
        this.connectors = new ArrayList<Element>();

        this.relations = new ArrayList<Relation>();
    }

    public ArrayList<ExtendedElement> getAllelements() {
        return this.allelements;
    }

    public void setAllelements(ArrayList<ExtendedElement> allelements) {
        this.allelements = allelements;
    }

    public ArrayList<ExtendedElement> getComponents() {
        return this.components;
    }

    public void setComponents(ArrayList<ExtendedElement> components) {
        this.components = components;
    }

    public ArrayList<ExtendedElement> getSubcomponents() {
        return this.subcomponents;
    }

    public void setSubcomponents(ArrayList<ExtendedElement> subcomponents) {
        this.subcomponents = subcomponents;
    }

    public ArrayList<ExtendedElement> getAllcomponents() {
        return this.allcomponents;
    }

    public void setAllcomponents(ArrayList<ExtendedElement> allcomponents) {
        this.allcomponents = allcomponents;
    }

    public ArrayList<Element> getConnectors() {
        return this.connectors;
    }

    public void setConnectors(ArrayList<Element> connectors) {
        this.connectors = connectors;
    }

    public ArrayList<Relation> getRelations() {
        return this.relations;
    }

    public void setRelations(ArrayList<Relation> relations) {
        this.relations = relations;
    }

}
