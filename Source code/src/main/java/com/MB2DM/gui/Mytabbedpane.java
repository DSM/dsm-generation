package com.MB2DM.gui;

import static com.MB2DM.Constants.Constants.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.JTabbedPane;

import com.MB2DM.UserCommand.UserCommand;

/**
 * Contains the DSM, DMM and MDM panels
 * 
 * @author Gabriel LUCAS
 * @author William PONS
 */
public class Mytabbedpane extends JTabbedPane {

    /** Constructor of the panels */
    public Mytabbedpane(History history, UserCommand uc) throws FileNotFoundException {
        super();

        ArrayList<String> list = new ArrayList<String>();
        list.add(DSM);
        list.add(DMM);
        list.add(MDM);

        for (String matrix : list) {
            this.add(matrix, new PanelGlobal(matrix, history, uc));
        }
    }

}
