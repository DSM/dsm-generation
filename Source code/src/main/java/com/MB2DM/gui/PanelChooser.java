package com.MB2DM.gui;

import static com.MB2DM.Constants.Constants.*;

import java.io.FileNotFoundException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.MB2DM.UserCommand.UserCommand;

/**
 * Panel for DSM, DMM or MDM
 * 
 * @author Gabriel LUCAS
 * @author William PONS
 */
public class PanelChooser extends JPanel {
    /**
     * Constructor of a DSM, DMM or MDM panel
     * 
     * @param uc      UserCommand that appeals the GUI
     * @throws FileNotFoundException
     */
    public PanelChooser(UserCommand uc, History history) throws FileNotFoundException {
        JButton buton = new JButton("Select Input File");
        buton.addActionListener(new ListenerFileChooser(uc, history));
        this.add(buton);
    }

}
