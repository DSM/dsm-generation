package com.MB2DM.gui;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.MB2DM.UserCommand.UserCommand;

import java.awt.event.*;

/**
 * Part of the window that displays the executed commands
 * 
 * @author Gabriel LUCAS
 * @author William PONS
 */
public class PanelHistory extends JPanel {

    /**
     * Constructor of the part of the window that displays the executed commands
     * 
     * @param history history to display
     * @param uc      UserCommand that appeals the GUI
     */
    public PanelHistory(History history, UserCommand uc) {
        super();
        BoxLayout b0 = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(b0);
        JLabel title = new JLabel("History", SwingConstants.CENTER);
        this.add(title);
        JScrollPane scrollableTextArea = new JScrollPane(history);
        scrollableTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        // Auto scroll
        scrollableTextArea.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());
            }
        });
        // Auto scroll end

        this.add(scrollableTextArea);
    }

}
