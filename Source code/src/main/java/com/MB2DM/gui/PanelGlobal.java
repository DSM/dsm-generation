package com.MB2DM.gui;

import static com.MB2DM.Constants.Constants.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import com.MB2DM.UserCommand.UserCommand;

/**
 * Panel for DSM, DMM or MDM
 * 
 * @author Gabriel LUCAS
 * @author William PONS
 */
public class PanelGlobal extends JPanel {

    /** Matrices types for each panel */
    private ArrayList<String> list_subname;

    /**
     * Constructor of a DSM, DMM or MDM panel
     * 
     * @param matrix  DSM, DMM or MDM
     * @param history history where are saved the executed commands
     * @param uc      UserCommand that appeals the GUI
     * @throws FileNotFoundException
     */
    public PanelGlobal(String matrix, History history, UserCommand uc) throws FileNotFoundException {
        super();

        ArrayList<String> list_subname = new ArrayList<String>();

        if (matrix.compareTo(DSM) == 0) {
            list_subname.add(FUNCTION_PART);
            list_subname.add(COMPONENT_PART);
            list_subname.add(REQUIREMENT_PART);
        } else if (matrix.compareTo(DMM) == 0) {
            list_subname.add(FUNCTION_PART + "_" + COMPONENT_PART);
            list_subname.add(FUNCTION_PART + "_" + REQUIREMENT_PART);
            list_subname.add(REQUIREMENT_PART + "_" + COMPONENT_PART);
            list_subname.add(REQUIREMENT_PART + "_" + FUNCTION_PART);
            list_subname.add(COMPONENT_PART + "_" + FUNCTION_PART);
            list_subname.add(COMPONENT_PART + "_" + REQUIREMENT_PART);
        } else { // MDM
            list_subname.add("");
        }

        BoxLayout b0 = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(b0);
        this.setAlignmentX(0);
        PanelModule pan;
        for (String subname : list_subname) {
            pan = new PanelModule(matrix, subname, history, uc);
            this.add(pan);
        }
    }

    public ArrayList<String> getList_subname() {
        return this.list_subname;
    }

    public void setList_subname(ArrayList<String> list_subname) {
        this.list_subname = list_subname;
    }

}
