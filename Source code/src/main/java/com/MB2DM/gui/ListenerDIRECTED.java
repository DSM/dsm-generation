package com.MB2DM.gui;

import static com.MB2DM.Constants.Constants.*;

import java.awt.event.*;

import javax.swing.JLabel;

import com.MB2DM.UserCommand.UserCommand;

/** Action to perform when button DIRECTED is pressed */
public class ListenerDIRECTED implements ActionListener {

    private String matrix;
    private String subname;
    private History history;
    private UserCommand uc;

    /**
     * Constructor whin initializes the class attributes
     * 
     * @param matrix  DSM, DMM or MDM
     * @param subname type of matrix
     * @param history history where are saved the previous commands
     * @param uc      UserCommand that appeals the GUI
     */
    public ListenerDIRECTED(String matrix, String subname, History history,  UserCommand uc) {
        this.matrix = matrix;
        this.subname = subname;
        this.history = history;
        this.uc = uc;
    }

    /** Action to perform when the button is pressed */
    public void actionPerformed(ActionEvent e) {

        if (matrix.compareTo(DSM) == 0) {

            if (subname.compareTo(FUNCTION_PART) == 0) {
                uc.FuntionsDirectedDSM();
            } else if (subname.compareTo(COMPONENT_PART) == 0) {
                uc.ComponentsDirectedDSM();
            } else { // Requirements
                uc.RequirementsDirectedDSM();
            }

        } else if (matrix.compareTo(DMM) == 0) {
            if (subname.compareTo(FUNCTION_PART + "_" + COMPONENT_PART) == 0) {
                uc.Functions_ComponentsDirectedDMM();
            } else if (subname.compareTo(FUNCTION_PART + "_" + REQUIREMENT_PART) == 0) {
                uc.Functions_RequirementsDirectedDMM();
            } else if (subname.compareTo(COMPONENT_PART + "_" + REQUIREMENT_PART) == 0) {
                uc.Components_RequirementsDirectedDMM();
            } else if (subname.compareTo(COMPONENT_PART + "_" + FUNCTION_PART) == 0) {
                uc.Components_FunctionsDirectedDMM();
            } else if (subname.compareTo(REQUIREMENT_PART + "_" + FUNCTION_PART) == 0) {
                uc.Requirements_FunctionsDirectedDMM();
            } else { // Requirements _ Components
                uc.Requirements_ComponentsDirectedDMM();
            }
        } else { // BOTH{
            uc.DirectedMDM();
        }

        JLabel txt1 = new JLabel(DIRECTED + " " + subname + " " + matrix);
        JLabel txt2 = new JLabel(uc.getMessage());

        history.add(txt1);
        history.add(txt2);
        history.revalidate();

    }
}