package com.MB2DM.gui;

import javax.swing.JFrame;

import com.MB2DM.UserCommand.UserCommand;

import java.io.FileNotFoundException;

import javax.swing.*;
/**
 * Main window, if exited, the programs stops
 * @author Gabriel LUCAS
 * @author William PONS
 */
public class MyFrame extends JFrame {

    boolean b;

    /**
     * Window Constructor
     * @param uc UserCommand that calls the GUI
     * @throws FileNotFoundException 
     */
    public MyFrame(UserCommand uc) throws FileNotFoundException {
        super("Design Matrices Generator");
        this.b=false;
        this.setSize(550, 700);
        History history = new History();
        Mytabbedpane tab = new Mytabbedpane(history,uc);
        this.add(tab);
        
        JPanel panelhistory = new PanelHistory(history,uc);
        this.add(panelhistory);

        
        
        BoxLayout b0 = new BoxLayout(getContentPane(),BoxLayout.Y_AXIS);
        setLayout(b0);
        this.setVisible(true);

        JPanel panelchooser = new PanelChooser(uc, history);
        this.add(panelchooser);
        
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                b=true;
            }
        });


    }


    public boolean getB() {
        return this.b;
    }



}
