package com.MB2DM.gui;

import java.awt.event.*;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JLabel;

import com.MB2DM.UserCommand.UserCommand;

/** Action to perform when button DIRECTED is pressed */
public class ListenerFileChooser implements ActionListener {

    private File folder;
    private File file;
    private History history;
    private UserCommand uc;

    /**
     * Constructor whin initializes the class attributes
     * 
     * @param matrix  DSM, DMM or MDM
     * @param subname type of matrix
     * @param history history where are saved the previous commands
     * @param uc      UserCommand that appeals the GUI
     */
    public ListenerFileChooser(UserCommand uc, History history) {
        this.file = null;
        this.folder = null;
        this.uc = uc;
        this.history = history;
    }

    /** Action to perform when the button is pressed */
    public void actionPerformed(ActionEvent e) {

        JFileChooser fileChooser = new JFileChooser();

        if (this.folder != null){
            fileChooser.setCurrentDirectory(folder);
        }

        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            this.file = fileChooser.getSelectedFile();

            uc.setFilename(this.file.getAbsolutePath());
            this.folder = this.file.getParentFile();
        }

        JLabel txt = new JLabel("Selected file : " + this.file.getName());

        history.add(txt);
        history.revalidate();
    }
}