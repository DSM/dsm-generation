package com.MB2DM.gui;

import static com.MB2DM.Constants.Constants.*;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.MB2DM.UserCommand.UserCommand;

/**
 * Part of a DSM, DMM or MDM panel that corresponds to a Matrix type
 * 
 * @author Gabriel LUCAS
 * @author William PONS
 */
public class PanelModule extends JPanel {

    /**
     * Constructor of Matrix type part in a DSM, DMM or MDM panel
     * 
     * @param matrix  DSM, DMM or MDM
     * @param subname name of the kind of matrix
     * @param history history where are saved executions
     * @param uc      UserCommand that appeals the GUI
     */
    public PanelModule(String matrix, String subname, History history, UserCommand uc) {

        String tot = subname;
        JLabel label = new JLabel();
        label.setPreferredSize(new Dimension(270, 30));
        label.setAlignmentX(0);
        label.setText(tot);
        this.add(label);
        JButton buton = new JButton(BINARY);
        buton.addActionListener(new ListenerBINARY(matrix, subname, history, uc));
        this.add(buton);
        JButton butoff = new JButton(DIRECTED);
        butoff.addActionListener(new ListenerDIRECTED(matrix, subname, history, uc));
        this.add(butoff);
        //JButton butdata = new JButton("Both");
        //butdata.addActionListener(new ListenerBOTH(matrix, subname, history, uc));
        //this.add(butdata);
        this.setAlignmentX(0);
    }

}
