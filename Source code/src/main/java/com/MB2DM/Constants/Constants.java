package com.MB2DM.Constants;

/**
 * Constants used in differents classes of the algorithm
 */
public class Constants {

    // Rootnode of TTool XML
    public static final String TURTLEMODELING = "TURTLEMODELING";

    // Children of the rootnode. Tag of the crux model types
    public static final String MODELING = "Modeling";

    // Child of MODELING. Mix of Block Definition and Internal Block Diagrams
    public static final String AVATAR_BLOCK_DIAGRAM_PANEL = "AVATARBlockDiagramPanel";

    // Child of MODELING. Requirement Diagram
    public static final String AVATAR_RD_PANEL = "AvatarRDPanel";

    // Child of MODELING. Use Case Diagram
    public static final String USE_CASE_DIAGRAM_PANEL = "UseCaseDiagramPanel";

    // "type" attribute value of MODELING children.
    public static final String AVATAR_DESIGN = "AVATAR Design";
    public static final String AVATAR_REQUIREMENT = "Avatar Requirement";
    public static final String AVATAR_ANALYSIS = "Avatar Analysis";

    // Use Case Diagram elements types

    public static final String BORDER = "702";
    public static final String ACTOR = "700";
    public static final String BOX_ACTOR = "703";
    public static final String USE_CASE = "701";
    public static final String ASSOCIATION = "110";
    public static final String INCLUDE = "111";
    public static final String EXTEND = "113";
    public static final String GENERALIZATION = "112";

    // Block Diagram elements types

    public static final String BLOCK_CRYPTO = "5000";
    public static final String DATA_TYPE = "5003";
    public static final String LIBRARY_CRYPTO = "5005";
    public static final String BDD_COMPOSITION = "5001";
    public static final String PORT_CONNECTION = "5002";

    // Requirement Diagram elements types

    public static final String REQUIREMENT = "5200";
    public static final String PROPERTY = "5201";
    public static final String ELEMENT_REFERENCE = "5207";
    public static final String REQUIREMENT_REFERENCE = "5208";
    public static final String REQ_COMPOSITION = "5205";
    public static final String DERIVE_REQT = "5608";
    public static final String COPY = "5204";
    public static final String REFINE = "5206";
    public static final String VERIFY = "5203";
    public static final String SATISFY = "5208";

    // Classifying elements

    public static final String TYPE = "type";
    public static final String COMPONENT = "COMPONENT";
    public static final String SUBCOMPONENT = "SUBCOMPONENT";
    public static final String CONNECTOR = "CONNECTOR";

    // Connection related information

    public static final String P1 = "P1";
    public static final String P2 = "P2";
    public static final String ID = "id";
    public static final String TG_CONNECTING_POINT = "TGConnectingPoint";
    public static final String INFOPARAM = "infoparam";
    public static final String VALUE = "value";
    public static final String FATHER = "father";
    public static final String EXTRAPARAM = "extraparam";
    public static final String OSO = "oso";
    public static final String OSD = "osd";
    public static final String ISD = "isd";
    public static final String ISO = "iso";
    public static final String SIGNAL = "Signal";

    // Classifying matrices

    public static final String FUNCTION_PART = "Functions";
    public static final String COMPONENT_PART = "Components";
    public static final String REQUIREMENT_PART = "Requirements";

    public static final String BINARY = "Binary";
    public static final String DIRECTED = "Directed";

    public static final String DSM = "DSM";
    public static final String DMM = "DMM";
    public static final String MDM = "MDM";

    // File extension

    public static final String XLSX = ".xlsx";
}
