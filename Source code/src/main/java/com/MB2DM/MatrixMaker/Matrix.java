package com.MB2DM.MatrixMaker;

import java.util.ArrayList;
import java.util.HashMap;

import com.MB2DM.Analyse.FullModel.FullModel;

public class Matrix {

    private ArrayList<ArrayList<String>> matrix;
    private String type;
    private String linksType;
    private String rowColumns;
    private String name;
    private HashMap<String, Integer> diagramsSize;

    /**
     * Constructor that creates an empty Matrix
     */
    public Matrix() {
        this.matrix = new ArrayList<ArrayList<String>>();
    }

    /**
     * Constructor that creates a ready-to-fill Matrix
     * 
     * @param rowNumber    number of rows
     * @param columnNumber number of columns
     * @param type         DSM, DMM or MDM
     * @param linksType    binary or directed
     */
    public Matrix(int rowNumber, int columnNumber, String type, String linksType) {
        this.matrix = new ArrayList<ArrayList<String>>();
        this.initialize(rowNumber, columnNumber);
        this.type = type;
        this.linksType = linksType;
        this.rowColumns = "";
        this.name = linksType + type;
    }

    /**
     * Constructor that creates a ready-to-fill Matrix, and enable to choose a name
     * 
     * @param rowNumber    number of rows
     * @param columnNumber number of columns
     * @param type         DSM, DMM or MDM
     * @param linksType    binary or directed
     * @param name         The name that is given to the Matrix
     */
    public Matrix(int rowNumber, int columnNumber, String type, String linksType, String name) {
        this.matrix = new ArrayList<ArrayList<String>>();
        this.initialize(rowNumber, columnNumber);
        this.type = type;
        this.linksType = linksType;
        this.rowColumns = "";
        this.name = name;
    }

    /**
     * Constructor that creates a ready-to-fill Matrix, enables to choose a name,
     * and save how many elements there are in functions, components, and
     * requirements
     * 
     * @param rowNumber    number of rows
     * @param columnNumber number of columns
     * @param type         DSM, DMM or MDM
     * @param linksType    binary or directed
     * @param name         The name that is given to the Matrix
     * @param fullModel    FullModel object that contains all the elements of the
     *                     model
     */
    public Matrix(int rowNumber, int columnNumber, String type, String linksType, String name, FullModel fullModel) {
        this.matrix = new ArrayList<ArrayList<String>>();
        this.initialize(rowNumber, columnNumber);
        this.type = type;
        this.linksType = linksType;
        this.rowColumns = "";
        this.name = name;
        this.diagramsSize = fullModel.getDiagramsSize();
    }

    /**
     * Creates a Matrix and fills it with empty characters
     * 
     * @param rowNumber    number of rows
     * @param columnNumber number of columns
     */
    public void initialize(int rowNumber, int columnNumber) {

        ArrayList<String> row = new ArrayList<String>();
        for (int i = 0; i < columnNumber; i++) {
            row.add("");
        }
        for (int i = 0; i < rowNumber; i++) {
            this.matrix.add(new ArrayList<String>(row));
        }
    }

    public ArrayList<ArrayList<String>> getMatrix() {
        return this.matrix;
    }

    public void setMatrix(ArrayList<ArrayList<String>> matrix) {
        this.matrix = matrix;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLinksType() {
        return this.linksType;
    }

    public void setLinksType(String linksType) {
        this.linksType = linksType;
    }

    public String getRowColumns() {
        return this.rowColumns;
    }

    public void setRowColumns(String rowColumns) {
        this.rowColumns = rowColumns;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, Integer> getDiagramsSize() {
        return this.diagramsSize;
    }

    public void setDiagramsSize(HashMap<String, Integer> diagramsSize) {
        this.diagramsSize = diagramsSize;
    }

}
