package com.MB2DM.MatrixMaker;

import static com.MB2DM.Constants.Constants.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.MB2DM.XMLTreatment.XMLAnalyser;

/**
 * Creates Domain Mapping Matrices from model data
 */
public class DMMMaker implements MatrixMaker {

    /** Contains the number of elements in Functions, Components and Requirements DSM */
    private HashMap<String, Integer> diagramsSize;

    /** Contains a MDM the DMM can be extracted from */
    private MDMMaker MDMMaker;

    /** List of binary DMM */
    private HashMap<String, Matrix> binaryMatrixList;

    /** List of directed DMM */
    private HashMap<String, Matrix> directedMatrixList;

    /**
     * Constructor of DMM matrices
     * 
     * @param analyse  an XMLAnalyser object containing all the information of the
     *                 model
     * @param MDMMaker the MDMMaker where are stored MDM where the DMM are extracted
     */
    public DMMMaker(XMLAnalyser analyse, MDMMaker MDMMaker) {

        this.diagramsSize = analyse.getFullModel().getDiagramsSize();
        this.MDMMaker = MDMMaker;
        this.binaryMatrixList = new HashMap<String, Matrix>();
        this.directedMatrixList = new HashMap<String, Matrix>();
    }

    /**
     * Creates binary and directed DMM
     */
    public void make() {
        this.makeBinary();
        this.makeDirected();
    }

    /**
     * Creates binary DMM by extracting relevant data from a MDM
     */
    public void makeBinary() {

        // Rows
        for (String keyRows : this.diagramsSize.keySet()) {

            // Columns
            for (String keyColumns : this.diagramsSize.keySet()) {

                // Not to create a DSM
                if (keyRows.compareTo(keyColumns) != 0) {

                    int rowSize = this.diagramsSize.get(keyRows);
                    int columnSize = this.diagramsSize.get(keyColumns);

                    Matrix matrix = new Matrix(rowSize + 1, columnSize + 1, DMM, BINARY);

                    // Functions / Components DMM
                    if (keyRows.compareTo(FUNCTION_PART) == 0 && keyColumns.compareTo(COMPONENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1; i < 1 + this.diagramsSize.get(FUNCTION_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    // Components / Functions DMM
                    else if (keyRows.compareTo(COMPONENT_PART) == 0 && keyColumns.compareTo(FUNCTION_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART); i < 1 + this.diagramsSize.get(FUNCTION_PART)
                                + this.diagramsSize.get(COMPONENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(1,
                                    1 + this.diagramsSize.get(FUNCTION_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    // Requirements / Functions DMM
                    else if (keyRows.compareTo(REQUIREMENT_PART) == 0 && keyColumns.compareTo(FUNCTION_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART); i < 1
                                + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                + this.diagramsSize.get(REQUIREMENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(1,
                                    1 + this.diagramsSize.get(FUNCTION_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }
                    // Functions / Requirements DMM
                    else if (keyRows.compareTo(FUNCTION_PART) == 0 && keyColumns.compareTo(REQUIREMENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1; i < 1 + this.diagramsSize.get(FUNCTION_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                            + this.diagramsSize.get(REQUIREMENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    // Components / Requirements DMM
                    else if (keyRows.compareTo(COMPONENT_PART) == 0 && keyColumns.compareTo(REQUIREMENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART); i < 1 + this.diagramsSize.get(FUNCTION_PART)
                                + this.diagramsSize.get(COMPONENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                            + this.diagramsSize.get(REQUIREMENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    // Requirements / Components DMM
                    else if (keyRows.compareTo(REQUIREMENT_PART) == 0 && keyColumns.compareTo(COMPONENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART); i < 1
                                + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                + this.diagramsSize.get(REQUIREMENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getBinaryMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    // Saves the new DMM
                    matrix.setRowColumns(keyRows + "_" + keyColumns);
                    matrix.setName(matrix.getRowColumns() + "_" + matrix.getLinksType() + "_" + matrix.getType());
                    this.binaryMatrixList.put(keyRows + "_" + keyColumns, matrix);
                }
            }
        }
    }

    /**
     * Creates directed DMM by extracting relevant data from a MDM
     */
    public void makeDirected() {

        for (String keyRows : this.diagramsSize.keySet()) {

            for (String keyColumns : this.diagramsSize.keySet()) {

                if (keyRows.compareTo(keyColumns) != 0) {

                    int rowSize = this.diagramsSize.get(keyRows);
                    int columnSize = this.diagramsSize.get(keyColumns);

                    Matrix matrix = new Matrix(rowSize + 1, columnSize + 1, DMM, DIRECTED);

                    if (keyRows.compareTo(FUNCTION_PART) == 0 && keyColumns.compareTo(COMPONENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1; i < 1 + this.diagramsSize.get(FUNCTION_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    else if (keyRows.compareTo(COMPONENT_PART) == 0 && keyColumns.compareTo(FUNCTION_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART); i < 1 + this.diagramsSize.get(FUNCTION_PART)
                                + this.diagramsSize.get(COMPONENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(1,
                                    1 + this.diagramsSize.get(FUNCTION_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    else if (keyRows.compareTo(REQUIREMENT_PART) == 0 && keyColumns.compareTo(FUNCTION_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART); i < 1
                                + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                + this.diagramsSize.get(REQUIREMENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(1,
                                    1 + this.diagramsSize.get(FUNCTION_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    else if (keyRows.compareTo(FUNCTION_PART) == 0 && keyColumns.compareTo(REQUIREMENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1; i < 1 + this.diagramsSize.get(FUNCTION_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                            + this.diagramsSize.get(REQUIREMENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    else if (keyRows.compareTo(COMPONENT_PART) == 0 && keyColumns.compareTo(REQUIREMENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART); i < 1 + this.diagramsSize.get(FUNCTION_PART)
                                + this.diagramsSize.get(COMPONENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                            + this.diagramsSize.get(REQUIREMENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }

                    else if (keyRows.compareTo(REQUIREMENT_PART) == 0 && keyColumns.compareTo(COMPONENT_PART) == 0) {

                        ArrayList<Integer> rows = new ArrayList<Integer>();
                        rows.add(0);
                        for (int i = 1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART); i < 1
                                + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART)
                                + this.diagramsSize.get(REQUIREMENT_PART); i++) {
                            rows.add(i);
                        }

                        int j = 0;
                        for (int i : rows) {
                            List<String> left = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(0, 1);
                            List<String> right = this.MDMMaker.getDirectedMDM().getMatrix().get(i).subList(
                                    1 + this.diagramsSize.get(FUNCTION_PART),
                                    1 + this.diagramsSize.get(FUNCTION_PART) + this.diagramsSize.get(COMPONENT_PART));
                            ArrayList<String> row = new ArrayList<String>();
                            row.addAll(left);
                            row.addAll(right);
                            matrix.getMatrix().set(j, row);
                            j++;
                        }

                    }
                    matrix.setRowColumns(keyRows + "_" + keyColumns);
                    matrix.setName(matrix.getRowColumns() + "_" + matrix.getLinksType() + "_" + matrix.getType());
                    this.directedMatrixList.put(keyRows + "_" + keyColumns, matrix);
                }
            }
        }
    }


    public HashMap<String,Integer> getDiagramsSize() {
        return this.diagramsSize;
    }

    public void setDiagramsSize(HashMap<String,Integer> diagramsSize) {
        this.diagramsSize = diagramsSize;
    }

    public MDMMaker getMDMMaker() {
        return this.MDMMaker;
    }

    public void setMDMMaker(MDMMaker MDMMaker) {
        this.MDMMaker = MDMMaker;
    }

    public HashMap<String,Matrix> getBinaryMatrixList() {
        return this.binaryMatrixList;
    }

    public void setBinaryMatrixList(HashMap<String,Matrix> binaryMatrixList) {
        this.binaryMatrixList = binaryMatrixList;
    }

    public HashMap<String,Matrix> getDirectedMatrixList() {
        return this.directedMatrixList;
    }

    public void setDirectedMatrixList(HashMap<String,Matrix> directedMatrixList) {
        this.directedMatrixList = directedMatrixList;
    }
   
}
