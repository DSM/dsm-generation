package com.MB2DM.MatrixMaker;

public interface MatrixMaker {

    /**
     * Should reuse makeBinary and makeDirected to create binary and directed matrices
     */
    public void make();

    /**
     * Should create a binary Matrix (or binary matrices)
     */
    public void makeBinary();

    /**
     * Should create a directed Matrix (or directed matrices)
     */
    public void makeDirected();

}
