package com.MB2DM.MatrixMaker;

import static com.MB2DM.Constants.Constants.*;

import com.MB2DM.Analyse.Relations.Relation;
import com.MB2DM.XMLTreatment.XMLAnalyser;

/**
 * Creates a Multiple Domain Matrix from XML model data
 */
public class MDMMaker implements MatrixMaker {

    /** Contains information about the model elements and their dependencies */
    private XMLAnalyser analyse;

    /**
     * Matrix containing binary relationships (0 if there is no dependency, 1
     * otherwise)
     */
    private Matrix binaryMDM;

    /**
     * Matrix containing directed relationships (the number of dependencies between
     * elements)
     */
    private Matrix directedMDM;

    /**
     * Constructor of a MDM matrix
     * 
     * @param analyse an XMLAnalyser object containing all the information of the
     *                model
     */
    public MDMMaker(XMLAnalyser analyse) {

        this.analyse = analyse;
    }

    /**
     * Creates a binary and a directed MDM, as Matrix objects
     */
    public void make() {
        this.makeBinary();
        this.makeDirected();
    }

    /**
     * Creates a binary MDM as a Matrix object
     */
    public void makeBinary() {
        // By default : FUNCTIONS / COMPONENTS / REQUIREMENTS

        // dimensions of the matrix
        int rowNumber = this.analyse.getFullModel().getDiagramsSize().get("Total");
        int columnNumber = rowNumber;

        // creates a matrix with good dimensions
        this.binaryMDM = new Matrix(rowNumber + 1, columnNumber + 1, MDM, BINARY, BINARY + "_" + MDM,
                analyse.getFullModel());

        // set headers names (first row and first column)
        this.binaryMDM.getMatrix().get(0).set(0, null);
        for (int i = 1; i < this.binaryMDM.getMatrix().get(0).size(); i++) {
            this.binaryMDM.getMatrix().get(0).set(i, this.analyse.getFullModel().getAllElements().get(i - 1).getName());
            this.binaryMDM.getMatrix().get(i).set(0, this.analyse.getFullModel().getAllElements().get(i - 1).getName());
        }

        // fill the matrix with "1" to represent dependencies (elements on rows depend on elements on columns)
        for (Relation relation : this.analyse.getFullModel().getRelations()) {
            int row = this.analyse.getFullModel().getAllElements().indexOf(relation.getElement2());
            int column = this.analyse.getFullModel().getAllElements().indexOf(relation.getElement1());

            if (row != column) {

                // port connector must be treated separately
                if (relation.getConnection().getConnector() != null && relation.getConnection().getConnector()
                        .getAttributeValue(TYPE).compareTo(PORT_CONNECTION) == 0) {
                    if (relation.getE1outputs() > 0) {
                        this.binaryMDM.getMatrix().get(row + 1).set(column + 1, "1");
                    }
                    if (relation.getE1outputs() > 0) {
                        this.binaryMDM.getMatrix().get(column + 1).set(row + 1, "1");
                    }
                }

                else {
                    if (relation.getE1outputs() > 0) {
                        this.binaryMDM.getMatrix().get(row + 1).set(column + 1, "1");
                    }
                }
            }

        }
    }

    /**
     * Creates a directed MDM as a Matrix object
     */
    public void makeDirected() {

        // dimensions of the matrix
        int rowNumber = this.analyse.getFullModel().getDiagramsSize().get("Total");
        int columnNumber = rowNumber;

        // creates a matrix with good dimensions
        this.directedMDM = new Matrix(rowNumber + 1, columnNumber + 1, MDM, DIRECTED, DIRECTED + "_" + MDM,
                analyse.getFullModel());

        // set headers names (first row and first column)
        this.directedMDM.getMatrix().get(0).set(0, "");
        for (int i = 1; i < this.directedMDM.getMatrix().get(0).size(); i++) {
            this.directedMDM.getMatrix().get(0).set(i,
                    this.analyse.getFullModel().getAllElements().get(i - 1).getName());
            this.directedMDM.getMatrix().get(i).set(0,
                    this.analyse.getFullModel().getAllElements().get(i - 1).getName());
        }

        // fill the matrix with numbers to represent dependencies (elements on rows depend on elements on columns)
        for (Relation relation : this.analyse.getFullModel().getRelations()) {
            int row = this.analyse.getFullModel().getAllElements().indexOf(relation.getElement2());
            int column = this.analyse.getFullModel().getAllElements().indexOf(relation.getElement1());

            if (row != column) {

                // port connector must be treated separately
                if (relation.getConnection().getConnector() != null && relation.getConnection().getConnector()
                        .getAttributeValue(TYPE).compareTo(PORT_CONNECTION) == 0) {

                    this.directedMDM.getMatrix().get(row + 1).set(column + 1,
                            Integer.toString(relation.getE1outputs()));
                    this.directedMDM.getMatrix().get(column + 1).set(row + 1,
                            Integer.toString(relation.getE2outputs()));
                }

                else {
                    this.directedMDM.getMatrix().get(row + 1).set(column + 1,
                            Integer.toString(relation.getE1outputs()));
                }
            }

        }
    }

    public XMLAnalyser getAnalyse() {
        return this.analyse;
    }

    public void setAnalyse(XMLAnalyser analyse) {
        this.analyse = analyse;
    }

    public Matrix getBinaryMDM() {
        return this.binaryMDM;
    }

    public void setBinaryMDM(Matrix binaryMDM) {
        this.binaryMDM = binaryMDM;
    }

    public Matrix getDirectedMDM() {
        return this.directedMDM;
    }

    public void setDirectedMDM(Matrix directedMDM) {
        this.directedMDM = directedMDM;
    }

}
