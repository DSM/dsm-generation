package com.MB2DM.MatrixMaker;

import com.MB2DM.Analyse.Relations.Relation;
import com.MB2DM.XMLTreatment.XMLAnalyser;

import static com.MB2DM.Constants.Constants.*;

/**
 * Creates Design Structure Matrices from XML model data
 */
public class DSMMaker implements MatrixMaker {

    /** contains the model that is parsed */
    private XMLAnalyser analyse;

    /** binary functuons DSM */
    private Matrix binaryUcdMatrix;

    /** directed functions DSM */
    private Matrix directedUcdMatrix;

    /** binary components DSM */
    private Matrix binaryBdMatrix;

    /** directed components DSM */
    private Matrix directedBdMatrix;

    /** binary requirements DSM */
    private Matrix binaryReqMatrix;

    /** directed requirements DSM */
    private Matrix directedReqMatrix;

    /**
     * Constructor of DSM matrices lists
     * 
     * @param analyse an XMLAnalyser object containing all the information of the
     *                model
     */
    public DSMMaker(XMLAnalyser analyse) {

        this.analyse = analyse;

        this.binaryUcdMatrix = null;
        this.binaryBdMatrix = null;
        this.binaryReqMatrix = null;

        this.directedUcdMatrix = null;
        this.directedBdMatrix = null;
        this.directedReqMatrix = null;

    }

    /**
     * Creates binary and directed DSM, as Matrix objects
     */
    public void make() {
        this.makeBinary();
        this.makeDirected();
    }

    /**
     * Creates binary DSM
     */
    public void makeBinary() {

        this.makeFunctionsBinary();
        this.makeComponentsBinary();
        this.makeRequiremmentsBinary();

    }

    /**
     * Creates a binary functions DSM
     */
    public void makeFunctionsBinary() {

        // creates an empty matrix with good dimensions
        int rowNumber = this.analyse.getFunctions().getAllelements().size();
        int columnNumber = rowNumber;
        this.binaryUcdMatrix = new Matrix(rowNumber + 1, columnNumber + 1, DSM, BINARY,
                FUNCTION_PART + "_" + BINARY + "_" + DSM);

        // set headers names (first row and first column)
        binaryUcdMatrix.getMatrix().get(0).set(0, "");
        for (int i = 1; i < this.binaryUcdMatrix.getMatrix().get(0).size(); i++) {
            this.binaryUcdMatrix.getMatrix().get(0).set(i,
                    this.analyse.getFunctions().getAllelements().get(i - 1).getName());
            this.binaryUcdMatrix.getMatrix().get(i).set(0,
                    this.analyse.getFunctions().getAllelements().get(i - 1).getName());
        }

        // fills the matrix with "1" to represent dependencies (elements on rows depend
        // on elements on columns)
        for (Relation relation : this.analyse.getFunctions().getRelations()) {
            int row = this.analyse.getFunctions().getAllelements().indexOf(relation.getElement2());
            int column = this.analyse.getFunctions().getAllelements().indexOf(relation.getElement1());
            if (row != column) {
                this.binaryUcdMatrix.getMatrix().get(row + 1).set(column + 1, "1");
            }

        }
    }

    /**
     * Creates a binary components DSM
     */
    public void makeComponentsBinary() {

        // creates an empty matrix with good dimensions
        int rowNumber = this.analyse.getComp().getAllelements().size();
        int columnNumber = rowNumber;
        this.binaryBdMatrix = new Matrix(rowNumber + 1, columnNumber + 1, DSM, BINARY,
                COMPONENT_PART + "_" + BINARY + "_" + DSM);

        // set headers names (first row and first column)
        this.binaryBdMatrix.getMatrix().get(0).set(0, "");
        for (int i = 1; i < this.binaryBdMatrix.getMatrix().get(0).size(); i++) {
            this.binaryBdMatrix.getMatrix().get(0).set(i, this.analyse.getComp().getAllelements().get(i - 1).getName());
            this.binaryBdMatrix.getMatrix().get(i).set(0, this.analyse.getComp().getAllelements().get(i - 1).getName());
        }

        // fills the matrix with "1" to represent dependencies (elements on rows depend
        // on elements on columns)
        for (Relation relation : this.analyse.getComp().getRelations()) {
            int row = this.analyse.getComp().getAllelements().indexOf(relation.getElement2());
            int column = this.analyse.getComp().getAllelements().indexOf(relation.getElement1());
            if (row != column) {

                // port connector are treated separately
                if (relation.getConnection().getConnector() != null && relation.getConnection().getConnector()
                        .getAttributeValue(TYPE).compareTo(PORT_CONNECTION) == 0) {

                    if (relation.getE1outputs() > 0) {
                        this.binaryBdMatrix.getMatrix().get(row + 1).set(column + 1, "1");
                    }
                    if (relation.getE2outputs() > 0) {
                        this.binaryBdMatrix.getMatrix().get(column + 1).set(row + 1, "1");
                    }
                }

                else {
                    if (relation.getE1outputs() > 0) {
                        this.binaryBdMatrix.getMatrix().get(row + 1).set(column + 1, "1");
                    }
                }
            }

        }
    }

    /**
     * Creates a requirements binary DSM
     */
    public void makeRequiremmentsBinary() {

        // creates an empty matrix with good dimensions
        int rowNumber = this.analyse.getRequirements().getAllelements().size();
        int columnNumber = rowNumber;
        this.binaryReqMatrix = new Matrix(rowNumber + 1, columnNumber + 1, DSM, BINARY,
                REQUIREMENT_PART + "_" + BINARY + "_" + DSM);

        // set headers names (first row and first column)
        this.binaryReqMatrix.getMatrix().get(0).set(0, "");
        for (int i = 1; i < this.binaryReqMatrix.getMatrix().get(0).size(); i++) {
            this.binaryReqMatrix.getMatrix().get(0).set(i,
                    this.analyse.getRequirements().getAllelements().get(i - 1).getName());
            this.binaryReqMatrix.getMatrix().get(i).set(0,
                    this.analyse.getRequirements().getAllelements().get(i - 1).getName());
        }

        // fills the matrix with "1" to represent dependencies (elements on rows depend
        // on elements on columns)
        for (Relation relation : this.analyse.getRequirements().getRelations()) {
            int row = this.analyse.getRequirements().getAllelements().indexOf(relation.getElement2());
            int column = this.analyse.getRequirements().getAllelements().indexOf(relation.getElement1());
            if (row != column) {
                this.binaryReqMatrix.getMatrix().get(row + 1).set(column + 1, "1");
            }

        }
    }

    /**
     * Creates directed DSM
     */
    public void makeDirected() {
        this.makeFunctionsDirected();
        this.makeComponentsDirected();
        this.makeRequiremmentsDirected();
    }

    /**
     * Creates a function directed DSM
     */
    public void makeFunctionsDirected() {

        // creates an empty matrix with good dimensions
        int rowNumber = this.analyse.getFunctions().getAllelements().size();
        int columnNumber = rowNumber;
        this.directedUcdMatrix = new Matrix(rowNumber + 1, columnNumber + 1, DSM, DIRECTED,
                FUNCTION_PART + "_" + DIRECTED + "_" + DSM);

        // set headers names (first row and first column)
        this.directedUcdMatrix.getMatrix().get(0).set(0, "");
        for (int i = 1; i < this.directedUcdMatrix.getMatrix().get(0).size(); i++) {
            this.directedUcdMatrix.getMatrix().get(0).set(i,
                    this.analyse.getFunctions().getAllelements().get(i - 1).getName());
            this.directedUcdMatrix.getMatrix().get(i).set(0,
                    this.analyse.getFunctions().getAllelements().get(i - 1).getName());
        }

        // fills the matrix with numbers to represent dependencies (elements on rows depend
        // on elements on columns)
        for (Relation relation : this.analyse.getFunctions().getRelations()) {
            int row = this.analyse.getFunctions().getAllelements().indexOf(relation.getElement2());
            int column = this.analyse.getFunctions().getAllelements().indexOf(relation.getElement1());
            if (row != column) {
                if (relation.getConnection().getConnector() != null && relation.getConnection().getConnector()
                        .getAttributeValue(TYPE).compareTo(PORT_CONNECTION) == 0) {

                    if (relation.getE1outputs() > 0) {
                        this.directedUcdMatrix.getMatrix().get(row + 1).set(column + 1, "1");
                    }
                    if (relation.getE2outputs() > 0) {
                        this.directedUcdMatrix.getMatrix().get(column + 1).set(row + 1, "1");
                    }
                }

                else {
                    if (relation.getE1outputs() > 0) {
                        this.directedUcdMatrix.getMatrix().get(row + 1).set(column + 1, "1");
                    }
                }
            }

        }
    }

    /**
     * Creates a components directed DSM
     */
    public void makeComponentsDirected() {

        // creates an empty matrix with good dimensions
        int rowNumber = this.analyse.getComp().getAllelements().size();
        int columnNumber = rowNumber;
        this.directedBdMatrix = new Matrix(rowNumber + 1, columnNumber + 1, DSM, DIRECTED,
                COMPONENT_PART + "_" + DIRECTED + "_" + DSM);

        // set headers names (first row and first column)
        this.directedBdMatrix.getMatrix().get(0).set(0, "");
        for (int i = 1; i < this.directedBdMatrix.getMatrix().get(0).size(); i++) {
            this.directedBdMatrix.getMatrix().get(0).set(i,
                    this.analyse.getComp().getAllelements().get(i - 1).getName());
            this.directedBdMatrix.getMatrix().get(i).set(0,
                    this.analyse.getComp().getAllelements().get(i - 1).getName());
        }

        // fills the matrix with numbers to represent dependencies (elements on rows depend
        // on elements on columns)
        for (Relation relation : this.analyse.getComp().getRelations()) {
            int row = this.analyse.getComp().getAllelements().indexOf(relation.getElement2());
            int column = this.analyse.getComp().getAllelements().indexOf(relation.getElement1());
            if (row != column) {
                if (relation.getConnection().getConnector() != null && relation.getConnection().getConnector()
                        .getAttributeValue(TYPE).compareTo(PORT_CONNECTION) == 0) {

                    if (relation.getE1outputs() > 0) {
                        this.directedBdMatrix.getMatrix().get(row + 1).set(column + 1,
                                Integer.toString(relation.getE1outputs()));
                    }
                    if (relation.getE2outputs() > 0) {
                        this.directedBdMatrix.getMatrix().get(column + 1).set(row + 1,
                                Integer.toString(relation.getE2outputs()));
                    }
                }

                else {
                    if (relation.getE1outputs() > 0) {
                        this.directedBdMatrix.getMatrix().get(row + 1).set(column + 1,
                                Integer.toString(relation.getE1outputs()));
                    }
                }
            }

        }
    }

    /**
     * Creates a requirement directed DSM
     */
    public void makeRequiremmentsDirected() {

        // creates an empty matrix with good dimensions
        int rowNumber = this.analyse.getRequirements().getAllelements().size();
        int columnNumber = rowNumber;
        this.directedReqMatrix = new Matrix(rowNumber + 1, columnNumber + 1, DSM, DIRECTED,
                REQUIREMENT_PART + "_" + DIRECTED + "_" + DSM);

        this.directedReqMatrix.getMatrix().get(0).set(0, "");
        for (int i = 1; i < this.directedReqMatrix.getMatrix().get(0).size(); i++) {
            this.directedReqMatrix.getMatrix().get(0).set(i,
                    this.analyse.getRequirements().getAllelements().get(i - 1).getName());
            this.directedReqMatrix.getMatrix().get(i).set(0,
                    this.analyse.getRequirements().getAllelements().get(i - 1).getName());
        }

        // fills the matrix with numbers to represent dependencies (elements on rows depend
        // on elements on columns)
        for (Relation relation : this.analyse.getRequirements().getRelations()) {
            int row = this.analyse.getRequirements().getAllelements().indexOf(relation.getElement2());
            int column = this.analyse.getRequirements().getAllelements().indexOf(relation.getElement1());
            if (row != column) {
                this.directedReqMatrix.getMatrix().get(row + 1).set(column + 1,
                        Integer.toString(relation.getE1outputs()));
            }
        }
    }

    public Matrix getBinaryUcdMatrix() {
        return this.binaryUcdMatrix;
    }

    public void setBinaryUcdMatrix(Matrix binaryUcdMatrix) {
        this.binaryUcdMatrix = binaryUcdMatrix;
    }

    public Matrix getDirectedUcdMatrix() {
        return this.directedUcdMatrix;
    }

    public void setDirectedUcdMatrix(Matrix directedUcdMatrix) {
        this.directedUcdMatrix = directedUcdMatrix;
    }

    public Matrix getBinaryBdMatrix() {
        return this.binaryBdMatrix;
    }

    public void setBinaryBdMatrix(Matrix binaryBdMatrix) {
        this.binaryBdMatrix = binaryBdMatrix;
    }

    public Matrix getDirectedBdMatrix() {
        return this.directedBdMatrix;
    }

    public void setDirectedBdMatrix(Matrix directedBdMatrix) {
        this.directedBdMatrix = directedBdMatrix;
    }

    public Matrix getBinaryReqMatrix() {
        return this.binaryReqMatrix;
    }

    public void setBinaryReqMatrix(Matrix binaryReqMatrix) {
        this.binaryReqMatrix = binaryReqMatrix;
    }

    public Matrix getDirectedReqMatrix() {
        return this.directedReqMatrix;
    }

    public void setDirectedReqMatrix(Matrix directedReqMatrix) {
        this.directedReqMatrix = directedReqMatrix;
    }

    public XMLAnalyser getAnalyse() {
        return this.analyse;
    }

    public void setAnalyse(XMLAnalyser analyse) {
        this.analyse = analyse;
    }

    public Matrix getUcdMatrix() {
        return this.binaryUcdMatrix;
    }

    public void setUcdMatrix(Matrix binaryUcdMatrix) {
        this.binaryUcdMatrix = binaryUcdMatrix;
    }

    public Matrix getBdMatrix() {
        return this.binaryBdMatrix;
    }

    public void setBdMatrix(Matrix binaryBdMatrix) {
        this.binaryBdMatrix = binaryBdMatrix;
    }

    public Matrix getReqMatrix() {
        return this.binaryReqMatrix;
    }

    public void setReqMatrix(Matrix binaryReqMatrix) {
        this.binaryReqMatrix = binaryReqMatrix;
    }

}
