# Model-based to Design Matrix (MB2DM)

Design Matrices Generation Tool

# Table of Contents

- [About MB2DM](#about-mb2dm)
    - [Citing](#citing)
- [Project Status](#project-status)
- [Functionalities](#functionalities)
- [How to Run](#how-to-run)
    - [Requirements](#requirements)
- [Development](#development)
    - [Packages](#packages)
    - [Output Format](#output-format)
- [License](#license)

# About MB2DM
[(Back to top)](#table-of-contents)

**MB2DM** is a tool to generate Design Matrices from xml files. The usage of Design Structure Matrices is widely applied to represent, cluster, and partition complex systems information for different purposes, one of them being systems design. Nevertheless, open-source software for their automatic creation is *rare*. This leads to manual workshop sessions for subject matter experts to fill in the design structure matrices, a practice that is very tedious and time consuming. The importance and application of Model Based System Engineering has increased over the years.
The presented tool generates design matrices while extracting information automatically from xml model file of the MBSE software [TTool](https://ttool.telecom-paris.fr/), it can also be adapted to extract information from [Papyrus](https://www.eclipse.org/papyrus/) models. 

The tool is able to create:
- Design Structure Matrix (DSM),
- Domain Mapping Matrix (DMM),
- Multi-Domain Matrix (MDM).

For more information refer to the paper "Design Structure Matrix Generation from Open-source MBSE Tools" available at: <https://ieeexplore.ieee.org/document/9582525>

## Citing
[(Back to top)](#table-of-contents)

When writing about MB2DM, cite the following paper:

**Plain Text**
```
W. Pons, S. Salas Cordero and R. Vingerhoeds, "Design Structure Matrix Generation from Open-source MBSE Tools", 
2021 IEEE International Symposium on Systems Engineering (ISSE), 2021, pp. 1-8, doi: 10.1109/ISSE51541.2021.9582525.
```

**BibTeX**
```
@INPROCEEDINGS{DSMgeneration2021,
  author={Pons, William and Salas Cordero, Sophia and Vingerhoeds, Rob},
  booktitle={2021 IEEE International Symposium on Systems Engineering (ISSE)}, 
  title={Design Structure Matrix Generation from Open-source MBSE Tools}, 
  year={2021},
  volume={},
  number={},
  pages={1-8},
  doi={10.1109/ISSE51541.2021.9582525}}
```
# Project Status
[(Back to top)](#table-of-contents)

![](https://img.shields.io/github/v/release/navendu-pottekkat/awesome-readme?include_prereleases)

![](https://img.shields.io/badge/OSS%20Status-Healthy-darkgreen.svg)


## Usage
[(Back to top)](#table-of-contents)

The proposed algorithm parses a desired TTool XML file (which describes a model), then stores the information and processes it to obtain DSMs, DMMs, and a MDM. The algorithm can create a Functions DSM using the Use Case Diagram in a model, a Components DSM using the Block Diagram, and a Requirement DSM using the Requirement Diagram. The generated DSMs can be either binary or numeric, as required by the user, the binary ones with "1" where there is a relation, or numeric ones when there is information about the number of inputs/outputs. The former could include directional or non-directional dependencies, depending on what is needed. The later would show directed dependencies. In TTool, only Port Connectors enable to define outputs and inputs through the relation. The MDM contains all the elements of Functions, Components and Requirements DSMs. Only one MDM is created per model. 

# How to run?
[(Back to top)](#table-of-contents)

No need to install. Just clone the repository or download the latest release. 

Go to the **Application** folder. It contains both a JAR file and an *.exe* file. The *.jar* file can be used with a Java Runtime Environment. The EXE can be used on Windows.

1. Choose either the .jar or .exe and click on it
2. Select input file 
3. Select the types of matrices you wish to obtain from the DSM, DMM or MDM tab accordindly 
4. A message on the history window will appear with the name of the selected matrix
5. The output file can be found on the automatically created at *Application\design_matrices*

Input example files can be found in [this](https://gitlab.telecom-paris.fr/mbe-tools/TTool/-/tree/master/modeling/AVATAR) TTool repository path, click [here](https://gitlab.telecom-paris.fr/mbe-tools/TTool/-/blob/master/modeling/AVATAR/MicroWaveOven_SafetySecurity_fullMethodo.xml) to find the *MicroWaveOven_SafetySecurity_fullMethodo.xml* referred to in the paper. 

If you want to create your own models to later use with **MB2DM** download [TTool](https://ttool.telecom-paris.fr/) and get to work. Additionaly, on TTool you can find model example files in the *TTool\modeling* folder.


## Requirements

- Java 8 

- Excel (The output files are set as a default to XLSX format, but you can refer to [this section](#output-format) if you want to the output files in a CSV format)

# Development
[(Back to top)](#table-of-contents)

The **Source Code** folder contains the source code of the project. It is a Java project created with Maven.
 
## Packages 

The **XMLTreatment** package parses the XML file with the JDOM API, and then categorize and store the data, using the **XMLParser** and **XMLAnalysis** classes. 

The **XMLAnalyser** interacts with the classes of the **Analyse** and **MatrixMaker** packages.  **XMLAnalyser** first calls the classes of **Analyse** to create the relations between the system elements and then calls the classes of **MatrixMaker** to create the matrices from both the elements and their dependencies. 

First, the **Analyse** package is used. If only a DSM is requested to be built, the classes of the **ModelParts** package within the **Analyse** package handle the request and map the dependencies of a DSM. These can be between functions, components, or requirements. It avoids building all the dependencies if they are not requested. If a DMM or a MDM is requested, then all the dependencies are created with the **FullModel** class. Both the **ModelParts** and **FullModel** classes use the **Relation** class in the package of the same name. This class enables to properly define every kind of dependency between system elements. Each dependency is composed of two members and a connection defined by the **Connection** class. At this point, are all the diagrams of a SysML model made in TTool are analysed and all their elements and dependencies have been created. To avoid redundancy, **ExtendedElement** gathers same type elements from different diagrams if they have same name.

Finally, the classes of the **MatrixMaker** package are called. The **Matrix** class defines the basis structure of a matrix as a list of lists containing strings of characters. The **DSMMaker** class uses the results of from a **Diagram** class to fill a Matrix with the Inputs of dependencies in rows, and the Outputs in columns. The **MDMMaker** class uses the information of the **FullModel** class to realise the same work. The **DMMMaker** class requires a MDM to be built. It extracts the relevant part of data from the MDM. The tables built are then given as arguments to methods of the **SheetWriter** class, that writes them to files. 

## Output Format
[(Back to top)](#table-of-contents)

If the desired format of the output file is not ".xlsx", there are 2 options:

- Use an online converter, or

- Change the code and recompile:

	Proceed to modify the following line in the **Constants.java** file found at *Source code\src\main\java\com\MB2DM\Constants*

	`public static final String XLSX = ".xlsx";`

	and verify *SheetWriter.java* file for consistency.



# License
[(Back to top)](#table-of-contents)

Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) 
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
